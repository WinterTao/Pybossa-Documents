# project.py

声明Project类，继承db.Model和DomainObject；  

## Project类:  

定义表名；  
定义列名及属性； 

### needs_password(self):
返回获取密码是否存在；

### get_passwd_hash(self):
返回密码的hash值；

### get_passwd(self):
如果需要密码，解码并返回密码的hash值；  
返回None；

### set_password(self, password):
如果password的长度大于1:  

* info的passwd_hash属性的值等于hash加密的password；
* 返回True

info的passwd_hash属性的值等于None；  
返回False；

### check_password(self, password):
如果需要密码，返回储存的密码是否与输入的密码相等；  
返回False；

### has_autoimporter(self):
返回是否能获取到不为None的自动导入器；  

### get_autoimporter(self):
获取自动导入器；  

### set_autoimporter(self, new=None):
设置自动导入器为输入的new值；  

### delete_autoimporter(self):
删除自动导入器；

### has_presenter(self):
如果任务的presenter已关闭，返回True；  
否则，返回任务的presenter是否为空；

### public_attributes(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；  

返回一个公共属性列表；  

### public_info_keys(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；

定义一个default列表；  
定义extra的值为当前app的设置中CATEGORY_INFO_PUBLIC_FIELDS的值；

如果extra存在，返回default和extra并集的列表；  
否则，返回default列表；


