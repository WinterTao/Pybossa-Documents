# event_listners

定义网页钩子队列webhook_queue；  
定义邮件队列mail_queue;  
定义网页推送队列webpush_queue;  

## add_blog_event(mapper, conn, target):
**装饰:** 在Blogpost进行insert操作之后执行被修饰函数；  

查询目标项目的信息；  
逐行读取查询到的数据，对其进行json化之后更新进obj字典；

将object更新进redis的feed流；

如果当前应用没有禁用邮件通知：

* 将notify_blog_users方法加入邮件的job队列mail_queue；
* 定义内容contents；
* 定义标头heading；  
* 定义链接地址launch_url；  
* 定义网页按钮web_buttons；
* 将push_notification方法添加到网页推送job队列webpush_queue；  

## add_project_event(mapper, conn, target):
**装饰:** 在Project进行insert操作之后执行被修饰函数；  

将目标的各种信息以字典形式录入tmp；  
定义字典obj；  
用tmp获取字典化的public的数据；  
将tmp的数据更新进obj；  
将obj更新进redis的feed流；  

为项目数据统计表project_stats插入一条空数据；  

## add_task_event(mapper, conn, target):
**装饰:** 在Task进行insert操作之后执行被修饰函数；

查询目标项目的信息；  
将查询到的信息录入obj； 
将obj更新进redis的feed流；  

## add_user_event(mapper, conn, target): 
**装饰:** 在User进行insert操作之后执行被修饰函数；

定义obj为目标字典化的public数据；  
将obj更新进redis的feed流；  

## add_user_contributed_to_feed(conn, user_id, project_obj):  
如果user_id存在：  

* 查询指定用户的信息
* 读取查询到的信息存入tmp；  
* 在tmp中加入项目的相关信息；  
* 将tmp更新到redis的feed流；  

## is_task_completed(conn, task_id, project_id):
查询指定项目的指定任务已完成的数量；  
查询指定任务需要完成的数量；  
返回完成的数量是否大于需要完成的数量；  

## push_webhook(project_obj, task_id, result_id):
如果输入的项目对象的webhook属性存在：  

* 定义payload；  
* 将webhook函数加入webhook_queue的job队列；  

## create_result(conn, project_id, task_id):
查询指定项目指定任务已完成的id；  
将所有id连接成字符串存入task_run_ids；  
查询指定项目指定任务获得的结果的id；

逐行读取查询到的数据results：  

* 如果数据存在: 
    * 更新结果对应id的最新版本属性为false；  

向result中插入一条新的数据；  
查询指定项目指定任务的最新结果；  
返回最新结果的id；  

## on_taskrun_submit(mapper, conn, target):
**装饰:** 在TaskRun进行insert操作之后执行被修饰函数；

查询目标项目的信息；  
读取查询到的数据并存入tmp；  

将tmp字典化的public数据更新到字典project_public中；  

添加目标用户参与到目标项目中；  
如果任务已经完成：  

* 更新任务状态；  
* 跟新redis的feed流；  
* 创建一个新的结果，并获取结果id；  
* 更新一个项目添加webhook信息；  
* 将目标任务和结果数据推送进webhook；  

## update_project(mapper, conn, target):
**装饰:** 在Blogpost、Task和TaskRun表进行insert和update操作之后执行被修饰函数；

更新项目时间戳；  

## update_timestamp(mapper, conn, target):
**装饰:** 在Webhook表进行update操作之后执行被修饰函数；

更新目标时间戳；  

## update_timestamp(mapper, conn, target):
**装饰:** 在Blogpost表进行update操作之后执行被修饰函数；

更新目标时间戳；  

## make_admin(mapper, conn, target):
**装饰:** 在User表进行insert操作之前执行被修饰函数；  

查询用户数量；  
如果用户数为0，目标的管理员属性为True；  

## create_zero_counter(mapper, conn, target): 
**装饰:** 在Task表进行insert操作之后执行被修饰函数；

给counter表插入一条数据；  

## delete_task_counter(mapper, conn, target):
**装饰:** 在Task表进行delete操作之后执行被修饰函数；

从counter表删除指定数据；  

## increase_task_counter(mapper, conn, target):
**装饰:** 在TaskRun表进行insert操作之后执行被修饰函数；

给counter表插入一条数据；  

## decrease_task_counter(mapper, conn, target):
**装饰:** 在TaskRun表进行delete操作之后执行被修饰函数；

给counter表插入一条数据；

