# category.py
声明Category类，继承db.Model和DomainObject；  

## Category类:  

定义表名；  
定义列名及属性；  

### public_attributes(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；  

返回一个公共属性列表；  

### public_info_keys(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；

定义一个空列表default；  
定义extra的值为当前app的设置中CATEGORY_INFO_PUBLIC_FIELDS的值；

如果extra存在，返回default和extra并集的列表；  
否则，返回空列表；

