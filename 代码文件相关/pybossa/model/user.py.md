# user.py

声明User类，继承db.Model，DomainObject和UserMixin；  

## User类:  

定义表名；  
定义列名及属性；

### get_id(self):
返回name;

### set_password(self, password):
设置passwd_hash的值为hash加密后的传入的password；  

### check_password(self, password):
如果passwd_hash存在，返回解码后的passwd_hash和传入password是否相同；  
返回False;  
 

### public_attributes(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；  

返回一个公共属性列表；  

### public_info_keys(self):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；

定义一个default列表；  
定义extra的值为当前app的设置中CATEGORY_INFO_PUBLIC_FIELDS的值；

如果extra存在，返回default和extra并集的列表；  
否则，返回default列表；


