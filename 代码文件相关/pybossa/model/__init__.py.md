# \__init__.py

定义log实例用于输出日志；  
声明DomainObject类，继承object；  

## DomainObject类：

### dictize(self):
将表内数据字典化；  

### info_public_keys(self, data=None):
声明字典类型输出out； 
 
如果data不存在，data等于表内数据字典化后的数据；  

以key遍历public_info_keys:

* 如果data存在info属性的值；  
* out关于key属性的值为，data中info属性的值关于key属性的值；  

返回out；

### to_public_json(self, data=None):
声明字典类型输出out； 

如果data不存在，data等于表内数据字典化后的数据；

以col遍历类的public_attributes()返回值：

* 如果col等于'info'，out关于col属性的值为data上public key连接的info里面的值；  
* 否则，out关于col属性的值为data在col属性上的值；  

返回out；

### public_attributes(self):
等待重写；

### public_info_keys(self):
等待重写；

### undictize(cls, dict_):
**装饰@classmethod:** 被修饰方法制作类中运行，不能再实例中应用；  

检查NotImplementedError；  
等待重写；

### \__str__(self):
定义输出形式；（一般用于Python3）

### \__unicode__(self):
定义输出形式；（一般用于Python2）

### make_timestamp():
返回年月日时间戳；  

### make_uuid():
返回基于伪随机数的ID；

### update_project_timestamp(mapper, conn, target):
更新指定项目的时间戳所在列的数据；  

### update_target_timestamp(mapper, conn, target):
更新目标表的指定目标在时间戳所在列上的数据；



