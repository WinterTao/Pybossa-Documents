# util.py

## last_flashed_message():
获取flash消息；  
如果消息长度大于0，返回最后一条消息；
否则，返回None；  

## form_to_json(form):
获取form的数据tmp；  
向tmp中添加errors和csrf键值；  
返回tmp；  

## user_to_json(user):
返回字典化后的user数据；  

## hash_last_flash_message():
定义空字典data；  
获取最新的flash消息message_and_status；  
如果最新的flash消息存在：  

* 定义data的flash键值为message_and_status[1]的值；
* 定义data的status键值为message_and_status[0]的值；  

json化data并用base64对其进行编码，将编码后得到的值返回；

## handle_content_type(data):
如果请求的标头包含的Content-Type的值为application/json或者request的参数的response_format键值为json：  

* 获取最新flash消息message_and_status；

* 如果message_and_status存在：
    * 定义data的flash键值为message_and_status[1]的值；
    * 定义data的status键值为message_and_status[0]的值；
* 遍历data中的键item：
    * 如果data的item键值是一个Form实例，将其json化；
    * 如果data的item键值是一个Pagination实例，将其json化；  
    * 如果item是announcement，将data的item键值json化；
    * 如果item是blogposts，将其中blog的public属性json化；  
    * 如果item是categories：  
        * 定义空列表tmp；
        * 遍历data的item键值cat：  
            * 如果cat是字典，将cat json化
            * 将cat添加到tmp中；
        * data的item键值等于tmp；
    * 如果item是active_cat：
        * 如果data的item键值不是字典，将其json化存入cat；
        * 定义data的item键值为cat；  
    * 如果item是users，且data的item键值不是str，将其json化；  
    * 如果item是users，或者projects，或者tasks，或者locs，且data的item键值是str，将data的item键值字典化；  
    * 如果item是found，将data的item键值json化；  
    * 如果item是category，将data的item键值中的public属性json化；  
* 如果data中包含'code'键，返回json化的data值和data的code键值；
* 否则，只返回json化的data值；  

否则：  

* 读取data的template键值template；  
* 删除data中的template键值；
* 如果'code'在data的键中：
    * 定义错误代码error_code为data的code键值；  
    * 删除data的code键值
    * 返回以data为参数租用的template模板，和错误代码；
* 否则，返回以data为参数租用的template模板；  

## redirect_content_type(url, status=None):
定义字典data；  
如果status不是空，data的status键值为status；  
如果请求的标头包含的Content-Type的值为application/json或者request的参数的response_format键值为json，基于data返回一个HTML模板或者json值；  
否则，重定向到url地址；  

## url_for_app_type(endpoint, _hash_last_flash=False, **values):
获取spa服务器名；  
如果服务器名存在：  

* 从values中删除'_external'的值；  
* 如果_hash_last_flash为真：
    * values的flash键值为最新的flash消息的hash值；
    * 返回spa服务器名加endpoint的地址；
* 返回spa服务器名加endpoint的地址；

返回endpoint的地址；  

## jsonpify(f):

定义装饰器decorated_function:

* 定义callback为请求的参数中callback的key值；
* 如果callback存在：
    * 定义content；
    * 返回当前应用的content响应；
* 否则，返回被装饰函数；

返回装饰器；

## admin_required(f):

定义装饰器decorated_function：  

* 如果当前用户是管理员，返回被装饰函数；  
* 否则，返回403错误；  

返回装饰器；

## pretty_date(time=False):

定义当前时间now；  
求当前时间和time的差diff；  
将差转化为秒second_diff；  
将差转化为天day_diff;  

如果day_diff小于0，返回''； 
 
如果day_diff等于0，根据不同秒差返回提示；

根据不同天差返回提示；

## Pagination类：  

### \__init__(self, page, per_page, total_count):
初始化page，per_page，total_count；

### def pages(self):
**装饰@property:** 将被修饰函数作为一个attribute进行访问；  

返回页数；

### has_prev(self):
**装饰@property:** 将被修饰函数作为一个attribute进行访问；

返回当前页是否大于1；  

### has_next(self):
**装饰@property:** 将被修饰函数作为一个attribute进行访问；

返回当前页是否为最终页；  

### iter_pages(self, left_edge=0, left_current=2, right_current=3, right_edge=0):

定义last为0；  
对所有页面进行迭代：  

* 如果num没超出范围：
    * 如果last+1不为num，yield返回None；  
    * yield返回num；
    * last等于num；    

### to_json(self):

将对象的各种属性字典化并返回；  


## unicode_csv_reader(unicode_csv_data, dialect=csv.excel, **kwargs):
获取unicode_csv_data中的数据csv_reader；  

逐行读取csv_reader，yield返回unicode列表化的row值；  

## utf_8_encoder(unicode_csv_data):
逐行读取unicode_csv_data的数据line，yield返回utf-8编码后的line；  

## UnicodeWriter类

### __init__(self, f, dialect=csv.excel, encoding="utf-8", **kwds):
定义queue，writer，stream，encoder；

### writerow(self, row):
定义空列表line；  
用s遍历row：

* 如果s的类型是字典，line中添加s的json值；
* 否则，line中添加s的utf-8编码；  

读取queue中的value，对其进行utf-8解码，在用自带编码器进行编码，然后写入stream；  
清空队列；

### writerows(self, rows): 
逐行读取rows，每行进行写入操作；  



## get_user_signup_method(user):
获取用户的登录方式并返回；  

## get_port():
获取端口值；  

## get_user_id_or_ip():
定义CryptoAn加密方法对象；  
获取user_id；  
获取user_ip；  
获取请求中的external_uid;  
以字典形式返回以上信息；  

## with_cache_disabled(f):
在被修饰函数执行期间停用缓存，并在执行完成后恢复本来的设置；  

## is_reserved_name(blueprint, name):
检查name是否已经被注册在blueprint这个蓝本里面；  

## username_from_full_name(username):
对username格式化；  

## rank(projects, order_by=None, desc=False):
对projects按照一定计分规则排序；  

## _last_activity_points(project):
按时间计算最近活动的得分；  

## _points_by_interval(value, weight=1):
按value的值设置分数并乘以权重得到最终得分；  

## publish_channel(sentinel, project_short_name, data, type, private=True):
向指定channel发布json数据；  

## fuzzyboolean(value):
将各种值转换为布尔值；  

## get_avatar_url(upload_method, avatar, container):
获取头像地址；  

## get_disqus_sso(user):
获取加密信息和密钥整理remote_auth_3信息和密钥一起写入网页脚本；

## get_disqus_sso_payload(user):
加密user信息；
返回base64编码的信息，时间戳，加密信息和密钥；

## exists_materialized_view(db, view):
检查数据库db是否存在物化视图view；  

## refresh_materialized_view(db, view):
刷新db中的物化视图view；  
返回刷新信息；

## check_password_strength(password, min_len=8, max_len=15, uppercase=True, lowercase=True, numeric=True, special=True, message=""):
检查密码强度；  


