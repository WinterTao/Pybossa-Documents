# blogpost.py
声明BlogpostAuth类，继承object；  

## BlogpostAuth类

定义_specific_actions；

### \__init__(self, project_repo):
初始化project_repo属性；   

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；  

### can(self, user, action, blogpost=None, project_id=None):
在action名前加_；  
返回相应action的方法；  

### \_create(self, user, blogpost=None, project_id=None):
如果用户匿名或者（blogpost和project_id都不存在），返回False；  
通过blogpost和project_id获取project数据；  
如果blogpost不存在，返回用户是否是项目的拥有者或管理员；  
返回blogpost的用户id是否与project的拥有者id的user的id匹配，或者用户是否为管理员；  

### \_update(self, user, blogpost, project_id=None):
如果用户匿名，返回False；
返回blogpost的用户id是否与user的id匹配，或者用户是否为管理员；  

### \delete(self, user, blogpost, project_id=None):
如果用户匿名，返回False；
返回blogpost的用户id是否与user的id匹配，或者用户是否为管理员；  

### \_get_project(self, blogpost, project_id):
如果blogpost存在，返回通过blogpost的project_id获得的项目数据；  
否则，返回通过project_id获得的项目数据；

### \_is_admin_or_owner(self, user, project):
返回用户不是匿名，且用户为管理员或者用户是项目拥有者


