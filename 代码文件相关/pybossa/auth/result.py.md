# result.py
声明ResultAuth类，继承object

## ResultAuth类:
定义_specific_action；  

### \__init__(self, project_repo):
初始化project_repo属性；  

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, result=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, result):
返回False；

### \_read(self, user, result=None):
返回True；

### \_update(self, user, result):
如果用户匿名，返回False；  
通过result和project_id获取项目数据；  
返回用户是否是项目拥有者或者管理员；  

### \_delete(self, user, result):
返回False；  

### \_get_project(self, result, project_id):
如果result存在，返回通过result的project_id获得的项目数据；  
否则，返回通过project_id获得的项目数据；

