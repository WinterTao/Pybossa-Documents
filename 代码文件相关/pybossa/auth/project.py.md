# project.py
声明ProjectAuth类，继承object；

## ProjectAuth类
定义_specific_action

### \__init__(self, task_repo, result_repo):
初始化属性task_repo和result_repo

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, taskrun=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, project=None):
如果project不存在，且用户已被授权，返回项目是否还没发布；  
返回用户是否被授权；

### \_read(self, user, project=None):
如果project不存在，且project还没发布，返回用户是否是管理员或者拥有该项目；
返回True；

### \_update(self, user, project):
返回用户是否是管理员或者拥有该项目；  

### \_delete(self, user, project):
如果通过项目id在result_repo中获得的数据存在，返回False；
返回用户是否是管理员或者拥有该项目；

### \_publish(self, user, project):
返回是否（项目有presenter，且项目里有任务，且用户是管理员或者拥有该项目）

### \_only_admin_or_owner(self, user, project):
返回用户不是匿名，且用户为管理员或者用户是项目拥有者

