# auditlog.py

声明AudilogAuth类，继承object;

## AuditlogAuth类

定义_specific_actions；  

### \__init__(self, project_repo):
初始化project_repo属性 

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；  

### can(self, user, action, auditlog=None, project_id=None):
在action名前加_；  
返回相应action的方法;  

### \_create(self, user, auditlog, project_id=None):
返回False；

### _read(self, user, auditlog=None, project_id=None):
如果用户是匿名或者（audirlog和project_id都不存在），返回False；  
通过auditlog和项目id获得项目project;  
返回用户是否为管理员或者用户是否拥有该项目；  

### \_update(self, user, auditlog, project_id=None):
返回False；  

### \_delete(self, user, auditlog, project_id=None):
返回False；  

### _get_project(self, auditlog, project_id):
如果auditlog存在，返回通过auditlog的project_id获得的项目数据；  
否则，返回通过project_id获得的项目数据；




