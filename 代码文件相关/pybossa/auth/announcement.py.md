# announcement.py

声明AnnouncementAuth类，继承object；  

## AnnouncementAuth类

定义_specific_actions；  

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；  

### can(self, user, action, announcement=None):
在action名前加_；  
返回相应action的方法；  

### \_create(self, user, announcement=None):
如果用户匿名或者没有announcement, 返回False;  
如果用户不是匿名而且用户是管理员，返回True；  
返回False；

### \_read(self, user, announcement=None):
返回True

### \_update(self, user, announcement):
如果用户匿名或者没有announcement, 返回False;  
如果用户不是匿名而且用户是管理员，返回True；  
返回False；  

## \_delete(self, user, announcement):
如果用户匿名或者没有announcement, 返回False;  
如果用户不是匿名而且用户是管理员，返回True；  
返回False；



