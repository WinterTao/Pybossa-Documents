# user.py
声明UserAuth类，继承object;  

## UserAuth类
定义_specific_actions;  

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, resource_user=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, resource_user=None):
返回用户已被授权且是管理员；  

### \_read(self, user, resource_user=None):
返回True；  

### \_update(self, user, resource_user):
如果用户匿名，返回False；  
返回用户是否对资源用户有create权限或者资源用户id与用户id匹配；

### \_delete(self, user, resource_user):
返回用户是否对资源用户有update权限；  


