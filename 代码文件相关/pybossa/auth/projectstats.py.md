# projectstats.py
声明ProjectStatsAuth类，继承object

### ProjectStatsAuth类
定义_specific_action

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, webhook=None, project_id=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, webhook, project_id=None):
返回False；

### \_read(self, user, webhook=None, project_id=None):
返回True；

### \_update(self, user, webhook, project_id=None):
返回False；

### \_delete(self, user, webhook, project_id=None):
返回False；

