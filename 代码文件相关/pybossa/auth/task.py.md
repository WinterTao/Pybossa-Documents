# task.py
声明TaskAuth类，继承object;  

## TaskAuth类
定义_specific_actions;  

### \__init__(self, project_repo, result_repo):
初始化属性project_repo和result_repo；  
### specific_actions(self):
*修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值； 

### can(self, user, action, task=None):
在action名前加_；  
返回相应action的方法； 

### \_create(self, user, task):
返回用户是否是管理员或者任务拥有者；

### \_read(self, user, task=None):
返回 True；

### \_update(self, user, task):
返回用户是否是管理员或者任务拥有者；

### \_delete(self, user, task):
如果用户是已被授权的管理员，返回True；  
如果通过任务id和任务的project_id获得的result数据存在，返回Flase；  
返回用户是管理员或者任务拥有者；  

### \_only_admin_or_owner(self, user, task):
如果用户匿名：

* 通过任务的project_id获得project数据；
* 返回用户是否是项目的拥有者或者管理员；  

返回False；

