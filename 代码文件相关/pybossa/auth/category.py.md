# category.py

声明AudilogAuth类，继承object;

## CategoryAuth类

定义_specific_actions；  

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；  

### can(self, user, action, category=None):
在action名前加_；  
返回相应action的方法;  

### \_create(self, user, category=None):  
返回管理员授权信息；

### \_read(self, user, category=None):
返回True；

### \_update(self, user, category):
返回管理员授权信息；

### \_delete(self, user, category):
返回管理员授权信息；

### \_only_admin(self, user):
返回用户已授权且用户为管理员；


