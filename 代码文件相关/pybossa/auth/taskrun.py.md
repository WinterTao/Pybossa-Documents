# taskrun.py
声明TaskRunAuth类，继承object；  

## TaskRunAuth类
定义_specific_action；  

### \__init__(self, task_repo, project_repo, result_repo):
初始化task_repo、project_repo和result_repo三个属性；  

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, taskrun=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, taskrun):
通过taskrun的project_id获取项目数据；  
如果用户匿名且项目不允许匿名用户操作，返回False；  
通过各种属性值获取在运行的任务数量是否小于等于0，将结果储存为授权信息authorized；  
如果没授权，返回403错误；
返回授权信息；

### \_read(self, user, taskrun=None):
返回True；

### \_update(self, user, taskrun):
返回False；  

### \_delete(self, user, taskrun):
如果用户匿名，返回False；  
通过taskrun的项目id和任务id获得结果数据；  
如果结果存在且taskrun的id和结果的task_run_id匹配，返回False；  
如果taskrun的用户id不存在，返回用户是否是管理员；  
返回用户是否是管理员，或者taskrun的用户id是否与user的id匹配；


