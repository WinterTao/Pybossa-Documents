# webhook.py
声明WebhookAuth类，继承object;  

## WebhookAuth类
定义_specific_actions;  

### \__init__(self, project_repo):
初始化project_repo属性； 

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, webhook=None, project_id=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, webhook, project_id=None):
返回False;  

### \_read(self, user, webhook=None, project_id=None):
如果用户匿名，或webhook和project_id都不存在，返回False；  
通过webhook和project_id获取project数据；  
返回用户是否是管理员或者项目拥有者；  

### \_update(self, user, webhook, project_id=None):
返回False；  

### \_delete(self, user, webhook, project_id=None):
返回False；  

### \_get_project(self, webhook, project_id):
如果webhook存在，返回通过webhook的project_id获得的project数据；  
返回通过project_id获得的项目数据；

