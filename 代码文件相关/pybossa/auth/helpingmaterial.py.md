# helpingmaterial.py

声明HelpingmaterialAuth类，继承object；  

## HelpingmaterialAuth类

定义_specific_actions；

### \__init__(self, project_repo):
初始化project_repo属性；    

### specific_action(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；  

### can(self, user, action, helpingmaterial=None, project_id=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, helpingmaterial=None, project_id=None):
如果用户匿名，或者helpingmaterial和project_id都不存在，返回False；  
通过helpingmaterial和project_id获得项目数据；  
如果helpingmaterial为空，返回用户是不是管理员或项目拥有者；  
返回用户是否是管理员或项目拥有者；  

### \_read(self, user, helpingmaterial=None, project_id=None):
如果helpingmaterial或项目id存在：

* 通过helpingmaterial和project_id过去项目数据project；
* 如果项目存在，返回项目是否已发布，或用户是否为管理员或项目拥有者；  
* 如果用户匿名，且helpingmaterial和project_id都不存在，返回False；  
* 返回用户是管理员或者项目拥有者；  

否则，返回True；

### \_update(self, user, helpingmaterial, project_id=None):

通过helpingmaterial和project_id获取项目数据project；  
如果用户匿名，返回False；  
返回用户是否是管理员或者项目拥有者；  

### \_delete(self, user, helpingmaterial, project_id=None):
通过helpingmaterial和project_id获取项目数据project；  
如果用户匿名，返回False；  
返回用户是否是管理员或者项目拥有者；  

### \_get_project(self, helpingmaterial, project_id):
如果helpingmaterial存在，返回通过helpingmaterial的project_id获得的项目数据；  
否则，返回通过project_id获得的项目数据；

### \_is_admin_or_owner(self, user, project):
返回用户不是匿名，且用户为管理员或者用户是项目拥有者

