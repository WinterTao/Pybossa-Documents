# \__init__.py

导入auth包中各组件，断言其存在；  
如果不存在，抛出异常；  
定义四种actions的集合；  
定义各种auth类的集合；  

## is_authorized(user, action, resource, **kwargs):

判断resource是否为类；  
如果为类，定义name获得类名，如果不是类，通过.__class__.__name__获取类名定义name；  
如果resource == 'token'，name定义为resource；  
为name设置验证器auth；  
为actions附加auth中特定的actions；  
断言action存在于actions中，若不存在返回错误信息；  
判断改用户是否有权利在这个resource下进行这个action；  

## ensure_authorized_to(action, resource, **kwargs):

获得当前用户是否有权在resource下进行这个action；  
如果没获得授权：

* 如果当前用户匿名，返回401错误；
* 否则，返回403错误；

返回授权信息；

## \_authorizer_for(resource_name):

声明空字典kwargs；  
根据resource_name的不同，对kwargs的内容进行更新；  
通过kwargs对resource_name指定的类进行定义；  

## handle_error(error):
将错误信息json化；  
将json化后的错误信息的状态码设置为401；  
返回json化的错误信息；

## jwt_authorize_project(project, payload):

如果payload不存在，返回错误信息；  
分割payload信息为list对象parts；  
如果parts[0]不是'bear'，返回错误信息；  
如果parts只包含一个元素，返回错误信息；  
如果parts包含多与两个元素，返回错误信息；  
（也就是说parts允许且只允许包含两个元素）  
用jwt方法通过project的密钥和H256算法解密parts的第二个元素定义为data；  

如果data中的project_id和项目id匹配且short_name和项目的short_name匹配，返回True;  
否则返回错误信息；

如果解码错误，返回错误信息；

