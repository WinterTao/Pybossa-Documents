# token.py
声明TokenAuth类，继承object

## TokenAuth类
定义_specific_actions

### specific_actions(self):
**修饰：**将方法变成属性，以使对象可以直接对其进行调用和定义；  
返回_specific_actions的值；

### can(self, user, action, _, token=None):
在action名前加_；  
返回相应action的方法；

### \_create(self, user, token=None):
返回False;  

### \_read(self, user, token=None):
返回用户是否匿名； 

### \_update(self, user, token):
返回False；  

### \_delete(self, user, token):
返回False；  

