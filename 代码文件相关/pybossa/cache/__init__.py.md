# \__init__.py

##  get_key_to_hash(*args, **kwargs):

定义key_to_hash;  
将参数组合起来；
返回key_to_hash;

## get_hash_key(prefix, key_to_hash):

将key_to_hash转换为utf-8编码；  
对key_to_hash进行md5加密并加上前缀生成key；  
返回key；  

## cache(key_prefix, timeout=300):
如果没有timeout参数，设置timeout参数为300；  

***

定义装饰器：  
将设置的REDIS_KEYPREFIX与输入参数key_prefix匹配储存在变量key中；  
如果环境变量里PYBOSSA_REDIS_CACHE_DISABLED的值为空（表示已启用缓存）：  

* 从sentinel的从服务器中获得key的输出缓存值output；
* 如果output存在，返回pickle读取后的output；  
* 将所装饰函数的返回值赋予output；  
* 在sentinel主服务器中设置key和pickle处理后的output值的key-value对，并且设置失效时间为timeout；  
* 返回output；  

将所装饰函数的返回值赋予output；  
在sentinel主服务器中设置key和pickle处理后的output值的key-value对，并且设置失效时间为timeout；  
返回output；

***

返回装饰器；

## memoize(timeout=300):
如果没有timeout参数，设置timeout参数为300；  

***

定义装饰器：  
将设置的REDIS_KEYPREFIX与输入参数被装饰函数的名字匹配储存在变量key中；  
将被修饰函数的传入参数保存为key_to_hash值；  
将key作为前缀和key_to_hash值配对；  
如果环境变量里PYBOSSA_REDIS_CACHE_DISABLED的值为空（表示已启用缓存）：

* 从sentinel的从服务器中获得key的输出缓存值output；
* 如果output存在，返回pickle读取后的output；  
* 将所装饰函数的返回值赋予output；  
* 在sentinel主服务器中设置key和pickle处理后的output值的key-value对，并且设置失效时间为timeout；  
* 返回output；

将所装饰函数的返回值赋予output；  
在sentinel主服务器中设置key和pickle处理后的output值的key-value对，并且设置失效时间为timeout；  
返回output；  

***

返回装饰器；

## delete_cached(key):
如果已启用缓存，将输入值key和redis前缀配对作为key，返回是否删除主服务器中关于key的缓存；  
返回True；  

## delete_memoized(function, *args, **kwargs)：

如果已启用缓存：

* 将redis前缀和输入函数名字配对组成key；  
* 如果存在其他输入参数：

    * 将传入参数连接成key_to_hash；
    * 将key和key_to_hash配对组成新的key；
    * 返回是否删除主服务器中关于key的缓存；

* 将符合sentinel从服务器中匹配key + '*'的key列表提取出来；  
* 如果不存在上述列表，返回False；  
* 返回是否删除sentinel主服务器中关于列表中所有key的缓存；  
返回True




