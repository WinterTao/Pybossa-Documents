# project_stats.py

开启数据库会话；

## n_tasks(project_id):
**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；  

返回指定项目的任务数量；

## stats_users(project_id, period=None):
**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；

定义users空字典；  
定义auth_users空列表；  
定义anon_users空列表；  

定义数据库查询参数params；  

流式查询关于已授权用户完成的任务次数的统计表，如果有设置period，则从period指定一段时间前的时间开始查询；  
读取查询到的数据并存到auth_users中；  

查询完成过任务的不同已授权用户的数量，如果设置有period，则从period指定一段时间前的时间开始查询；  
读取查询到的数据并存到users的n_auth属性中； 

流式查询关于匿名用户完成的任务次数的统计表，如果有设置period，则从period指定一段时间前的时间开始查询；  
读取查询到的数据并存到anon_users中；  

查询完成过任务的不同匿名用户的数量，如果设置有period，则从period指定一段时间前的时间开始查询；  
读取查询到的数据并存到users的n_anon属性中；

返回users，anon_users，auth_users；  

## convert_period_to_days(period):

将输入的period转换为天数存入int_period；  
如果报错int_period = 0；  
返回int_period；  

## stats_dates(project_id, period='15 day'):

**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；  

定义dates，dates_anon和dates_auth；  

将指定项目的任务数存入缓存；  
定义数据库查询参数params；  

流式查询指定期间内已完成的项目；  
逐行读取查询到的结果获取日期信息:

* 如果该为key已在dates中存在，则该key的值加一； 
* 否则添加定义这个key的值为1；  

```text
内部函数：_fill_empty_days(days, obj):

如果days的长度小于period转换为天数的长度：  
* 设置base为今天的时间；
* 循环x值从0到period天数:  
    * 定义temp_date为base减去x天的时间；
    * 如果temp_date不在days中，设置obj的temp_date属性为0；  
返回obj
```

为dates用0补充没完成任务的日期；  

查询每天授权用户完成的任务数；  
读取查询到的数据并将其存入dates_auth；  
为dates_auth用0补充没有完成任务的日期；  

查询每天匿名用户完成的任务数；  
读取查询到的数据并将其存入dates_anon；  
为dates_anon用0补充没有完成任务的日期；

返回dates，dates_anon，dates_auth；  

## stats_hours(project_id, period='2 week'):

**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；

定义空字典hours，hours_anon，hours_auth；  
定义max_hours，max_hours_anon，max_hours_anon为0；  

初始化hours，hours_anon，hours_auth对应的24小时key的值为0；  

定义数据库查询参数params；  

流式查询每个小时完成的任务的数量；  
读取查询到的数据存入hours；

流式查询完成最多任务的小时完成的任务数；  
将查询到的任务数存入max_hours；

流式查询每个小时已授权用户完成的任务的数量；  
读取查询到的数据存入hours_auth；

流式查询已授权用户完成最多任务的小时完成的任务数；  
将查询到的任务数存入max_hours_auth；

流式查询每个小时匿名用户完成的任务的数量；  
读取查询到的数据存入hours_anon；

流式查询匿名用户完成最多任务的小时完成的任务数；  
将查询到的任务数存入max_hours_anon；

返回hours, hours_anon, hours_auth, max_hours, max_hours_anon, max_hours_auth

## stats_format_dates(project_id, dates, dates_anon, dates_auth):
**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；  

将日期统计数据json化；  

## stats_format_hours(project_id, hours, hours_anon, hours_auth,max_hours, max_hours_anon, max_hours_auth):

**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；  

将小时统计数据json化；

## stats_format_users(project_id, users, anon_users, auth_users):

**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为一天；  

将用户统计数据json化；

## update_stats(project_id, period='2 week'):

更新指定项目指定期间的统计数据； 

## get_stats(project_id, period='2 week', full=False):
**装饰@memoize:** 将被装饰函数的数据存入缓存服务器并设置失效时间为五分钟；  

获取项目统计中指定项目的第一条数据存为ps；  
如果full为True，返回ps；  
否则，返回ps的日期统计数据，小时统计数据和用户统计数据




