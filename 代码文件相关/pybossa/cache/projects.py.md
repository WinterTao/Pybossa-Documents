# project.py

开启从数据库会话；  

## get_top(n=4):
**装饰@cache:** 以front_page_top_projects为key的前缀，并设置好category的缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  

选出运行任务数量前n的项目；

## browse_tasks(project_id, limit=10, offset=0):

从关于一个任务的答案数和运行次数的表中选出第offset+1条数据开始的limit条数据；  

定义tasks；  

逐行读取选取的数据：

* 将数据字典化；
* 设置运行数的答案数的占比存为task的pct_status；
* 将task添加进tasks列表；

返回tasks列表；

## \_pct_status(n_task_runs, n_answers): 

如果n_answers不等于0且答案数存在：  

* 如果n_task_run大于n_answers， 返回1.0;  
* 否则返回n_task_run/n_answers;  

返回0.0

## n_tasks(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；  

获得指定项目的任务数结果result；

定义n_tasks；  
提取result的数据并将任务数赋予n_tasks；  
返回n_tasks；  

## n_completed_tasks(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

获得指定项目已完成的任务数量；  
读取获得的数据并返回已完成的任务数量；  

## n_results(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；  

获得指定项目的结果数量；  
读取获得的数据并返回结果数；

## n_registered_volunteers(project_id):
**装饰@memoize:** 以REGISTERED_USERS_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；  

获取注册志愿者的数量；  
读取获得的数据并返回已注册志愿者数量；

## n_anonymous_volunteers(project_id):
**装饰@memoize:** 以REGISTERED_USERS_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

获取匿名志愿者的数量；  
读取获得的数据并返回匿名志愿者的数量；

## n_volunteers(project_id):
返回注册志愿测的数量加匿名志愿者的数量，这个和就是总共的志愿者数量；  

## n_task_runs(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

获取运行过的任务的数量；  
读取获得的数据并返回正在运行的任务数量；

## overall_progress(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

如果指定项目的任务数不为0，返回完成的数量除以任务的总数；  
否则，返回0；

## last_activity(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

选出最后一次运行的任务；  
读取获得的数据；  
如果任务存在，返回任务；  
否则，返回None；  

## average_contribution_time(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

获得完成任务的平均时间；  
读取平均时间；  
如果存在平均时间，将平均时间转换为秒并返回；  
否则，返回0；  

## n_blogposts(project_id):
获取指定项目的blogposts数量；  
读取获得的数据并返回博客发布数量；  

## \_n_featured(): 
**装饰@cache：**以number_featured_projects为key的前缀，并设置STATS_FRONTPAGE_TIMEOUT为缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  

查询被特征化的项目数量；  
读取获得的数据并返回被特征化的项目数量；  

## get_all_featured(category=None):
**装饰@memoize:** 以STATS_FRONTPAGE_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

查询被特征化的项目的相关信息以及关联用户的相关信息；  
读取查询到的数据并将数据json化然后返回；  

## get_featured(category=None, page=1, per_page=5):
获取指定某几页的被特征化的项目数据；  

## n_published():
**装饰@cache：**以number_published_projects为key的前缀，并设置STATS_APP_TIMEOUT为缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  

查询已发布的项目数量；  
读取查询到的数据并返回已发布的项目数量；

## \_n_draft():
**装饰@cache：**以number_draft_projects为key的前缀，并设置STATS_DRAFT_TIMEOUT为缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  

查询未发布的项目数量；  
读取查询到的数据并返回未发布的项目数量；  

## get_all_draft(category=None):
**装饰@memoize:** 以STATS_FRONTPAGE_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；  

查询未发布的项目的相关信息以及其关联的用户的相关信息；  
读取查询到的数据，将其json化，并返回；  

## get_draft(category=None, page=1, per_page=5):
获取指定某几页的未发布项目数据并返回；

## n_count(category):
**装饰@memoize:** 以N_APPS_PER_CATEGORY_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

如果类别为featured，返回被特征化的项目数量；  
如果类别为draft，返回未发布的项目的数量；  

查询特定类别short_name的已发布的项目的数量；  
读取查询到的数据并返回项目数量；  

## get_all(category):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

查询特定类别short_name的已发布项目相关消息以及其关联的用户的相关信息；  
读取查询到的数据并json化，然后返回；  

## get(category, page=1, per_page=5):

返回特定类别某几页的已发布项目的数据；  

## get_from_pro_user():

查询高级用户的项目相关信息；  
读取查询到的数据并返回高级用户的项目相关信息；  

## reset():
删除各种缓存数据；  

## clean_project(project_id, category=None):
删除指定项目的所有缓存数据；

## get_project_report_projectdata(project_id):
**装饰@memoize:** 以APP_TIMEOUT的值为缓存过期时间，对被修饰函数的返回数据进行缓存操作；

查询并统计指定项目的各项数据；  
读取统计的数据并将其返回；


