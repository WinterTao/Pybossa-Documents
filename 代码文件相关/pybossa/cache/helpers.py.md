# helpers.py

开启从数据库会话；  

## n_available_tasks(project_id, user_id=None, user_ip=None):

**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为三小时；

如果有user_id而且没有user_ip：  

* 定义sql语句'查询已完成的没运行过的指定项目的未完成的任务的数量'
* 执行sql语句并限定条件让项目id等于指定id，用户id等于指定id

否则：  

* 如果没有user_ip，定义user_id为'127.0.0.1'；  
* 定义sql语句'查询已完成的没有在运行的指定项目的未完成的任务的数量'
* 执行sql语句并限定条件让项目id等于指定id，用户ip等于指定ip；  

提取查询结果，返回n_tasks列的值；

## check_contributing_state(project, user_id=None, user_ip=None, external_uid=None, ps=None):

提取项目id；  
提取项目发布状态published；  
定义一个状态元组；  

如果没有传入ps，提取ProjectStats表中匹配项目id的第一条数据作为ps；  

如果ps的overall_progress不小于100， 返回completed状态；

如果published为False:

* 如果项目没有presenter或者没有task，返回draft状态；  
* 返回publish状态；  

如果可用项目存在，返回can_contribute状态；  
返回cannot_contribute状态；  

## add_custom_contrib_button_to(project, user_id_or_ip, ps=None):

如果project不是字典，将其字典化；
  
让project的contrib_button的值与check_contributing_state的返回状态一致；  

如果没有传入ps，提取ProjectStats表中匹配项目id的第一条数据作为ps；  

设置project的n_blogposts值等于ps的n_blogposts值；  
设置project的n_results值等于ps的n_results值；  

返回project；  

## has_no_presenter(project):
如果当前应用关闭了task_presenter, 返回False；  
否则：  

* 定义空presenter，返回project的has_presenters的相反真值；
* 如果遇到属性错误：
    * 返回project的info属性中的task_presenter是否在empty_presenters中；  
    * 如果遇到属性错误，返回True；  

## \_has_no_tasks(project_id):    
设置sql语句"计算指定project中任务的数量"；  
执行sql语句并限定project_id为传入值；  
提取所得数据；  
返回数量是否为0；  

