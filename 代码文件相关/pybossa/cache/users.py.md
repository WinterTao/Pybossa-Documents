# users.py

开启从数据库会话；  

## get_leaderboard(n, user_id=None, window=0, info=None):

返回排名前n位的用户；  
如果出现程序错误，回滚数据库，刷新或创建物化视图，返回排名前n位的用户；  

## get_user_summary(name):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；  

查询指定用户名的用户的数据总结；  
读取查询到的数据并已字典形式存入user；  
如果用户存在：

* 定义rank_score为用户排名得分；
* 定义用户user的rank属性值为rank_score的rank属性值； 
* 定义用户user的score属性值为rank_score的score属性值；
* 定义用户user的total属性为服务器上的总用户数；  

否则，返回None；

## public_get_user_summary(name):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；  

定义private_user为指定名字的用户的总结数据；  
声明public_user；  
如果private_user存在：  

* 声明User对象u；  
* 定义public_user为对private_user的数据进行json化后的数据；  

返回public_user；  

## rank_and_score(user_id):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

如果数据库不存在物化视图'users_rank'，创建物化视图；

查询用户排名中指定user的数据；  
读取查询到的数据，并题取用户的排名rank和得分score进行返回；  

## projects_contributed(user_id, order_by='name'):

查询指定用户最近一次参与的项目的数据；  
将查询到的项目的数据进行相关统计并返回；

## projects_contributed_cached(user_id, order_by='name'):  
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；  

返回指定用户最近参与的项目的相关数据；  

## public_projects_contributed(user_id):

对指定用户最近参与的项目的相关数据进行json化处理并返回；  

## public_projects_contributed_cached(user_id):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

对指定用户最近参与的项目的相关数据进行json化处理并返回；

## published_projects(user_id):
查询指定用户拥有的已发布的项目；  
将查询到的项目的数据进行相关统计并返回；

## published_projects_cached(user_id):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

返回指定用户拥有的已发布的项目数据；  

## public_published_projects(user_id):
对指定用户拥有的已发布的项目的数据进行json化处理；

## public_published_projects_cached(user_id):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

对指定用户拥有的已发布的项目的数据进行json化处理；

## draft_projects(user_id):
查询指定用户拥有的未发布的项目；  
将查询到的项目的数据进行相关统计并返回；

## draft_projects_cached(user_id):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

返回指定用户拥有的未发布的项目数据；

## get_total_users():
**装饰@cache：**以site_total_users为key的前缀，并以USER_TIMEOUT的值为缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  

返回服务器中用户的总数；

## get_users_page(page, per_page=24):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为USER_TIMEOUT的值；

定义offset为(page-1)*per_page；  

查询按用户创建顺序排序的完成过任务的用户中从offset+1起per_page个用户的数据；  
逐行以字典的形式读取查询到的数据并将其json化之后，将得到的字典列表返回；  

## delete_user_summary_id(oid):
删除缓存中的用户概要数据；

## delete_user_summary(name):
删除缓存中的用户概要数据；

## get_project_report_userdata(project_id):  
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为APP_TIMEOUT的值；

如果项目id不存在，返回None；  

统计项目的任务数量total_tasks；  

查询参与项目的用户的统计数据；  
读取查询到的数据并返回用户的统计数据；  

## get_user_pref_metadata(name):
**装饰@memoize：**将被装饰函数所得数据存入缓存并设置超时时间为APP_TIMEOUT的值；

查询指定用户的设置信息元数据；  

读取第一条数据并储存到upref_mdata中；  
返回upref_mdata；  

## delete_user_pref_metadata(name):
删除指定用户的设置信息元数据缓存；


