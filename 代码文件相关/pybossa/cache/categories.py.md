# categories.py

建立从数据库会画；

## get_all():

**装饰@cache：**以categories_all为key的前缀，并设置好category的缓存超时时间，对被装饰函数所得到的数据进行缓存操作； 
 
返回所有categories；  

## get_used():
**装饰@cache：**以categories_all为key的前缀，并设置好category的缓存超时时间，对被装饰函数所得到的数据进行缓存操作；  
  
设置sql语句：“查询project中已经用到的所有category并按category的id分组”; 
  
执行sql语句；  
将得到的数据存为一个字典的list；  
返回这个list；  

## reset():
删除所有categories的缓存；
删除已经在project中使用过的categories的缓存；


