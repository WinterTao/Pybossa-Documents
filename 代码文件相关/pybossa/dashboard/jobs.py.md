# jobs.py

## \_exists_materialized_view(view):
定义sql语句"查询pg_class中relname列是否存在relname=view的数据"；  
在从数据库执行附加条件为view=view的上述sql语句；  
提取执行结果并返回；  
返回False；  

## \_refresh_materialized_view(view): 
定义sql语句"刷新view的物化视图";  
执行sql语句；  
提交执行结果；  
返回信息'物化视图已刷新'；  

## active_users_week():
如果'dashboard_week_users'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；  

## active_anon_week():
如果'dashboard_week_anon'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；  

## draft_projects_week():
如果'dashboard_week_project_draft'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；  

## published_projects_week():
如果'dashboard_week_project_published'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；

## update_projects_week():
如果'dashboard_week_project_update'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；

## new_tasks_week():
如果'dashboard_week_new_task'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；

## new_task_runs_week():
如果'dashboard_week_new_task_run'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；

## new_users_week():
如果'dashboard_week_new_users'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；

## returning_users_week():
如果'dashboard_week_returning_users'存在物化视图，刷新物化视图；  
否则，创建物化视图并提交；  
返回信息'物化视图已创建'；


