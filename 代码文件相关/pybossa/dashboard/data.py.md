# data.py

## \_select_from_materialized_view(view, n_days=None):

如果没有设置n_days，设置sql语句“查询view的所有内容”，并定义options为空字典；  
否则，设置sql语句为“查询view中所有n_days等于传入的n_days的内容”，并定义options；

开启从数据库会话；  
返回sql在options下的执行结果；
如果出现程序错误，对从数据库rollback；  

## format_users_week():
从'dashboard_week_users'读取数据；  
返回附加时间戳的n_users列的数据；  

## format_anon_week():
从'dashboard_week_anon'读取数据；  
返回附加时间戳的n_users列的数据；

## format_new_tasks():
从'dashboard_week_new_task'读取数据；  
返回附加时间戳的day_tasks列的数据；

## format_new_task_run():
从'dashboard_week_new_task_run'读取数据；  
返回附加时间戳的day_task_runs列的数据；

## format_new_users():
从'dashboard_week_new_users'读取数据；  
返回附加时间戳的day_users列的数据；

## format_returning_users():
定义formatted_users为包含labels和series的字典；  
对1到7进行迭代：  

* 当迭代到1时，设置label为'1 day'；
* 否则，设置label为'i days'；  
* 从 'dashboard_week_returning_users' 表读取n_days列为i的数据；  
* 从读取到的数据中提取count列的数据并加上label得到formatted_data；  
* 将formatted_data的labels和series赋予formatted_users；  

返回formatted_users；  

## format_draft_projects():
从'dashboard_week_project_draft'读取数据；  
返回格式化后的数据；

## format_published_projects():
从'dashboard_week_project_published'读取数据；  
返回格式化后的数据；  

## format_update_projects():
从'dashboard_week_project_update'读取数据；  
返回格式化后的数据；

## \_graph_data_from_query(results, column, label=None):   
定义labels和series;  
逐行提取results中的数据：

* labels后面附加数据label或者提取出的日期数据；  
* series后面附加提取出的数据的column列的数据；  

如果labels为空，labels后面附加数据label或者当前日期的时间戳；  
如果series为空，添加一个0；  

返回labels和series组成的字典；

## \_format_projects_data(results):
定义formatted_projects；  
逐行读取results数据，将每列数据做成一个字典的形式附加在formatted_projects列表中；  
返回formatted_projects；


