# feed.py

## update_feed(obj):

sentinel在主服务器创建管道；  
将数据序列化；  
在管道中以时间为score添加序列化后的对象；  
执行pipeline；  

## get_update_feed():

从从服务器获得feed流的序列数据data；  
定义空列表feed；  
逐行读取data：  

* 用pickle载入成员赋予tmp变量；  
* 定义tmp的update属性为获得的score；  
* 如果tmp有info属性的值且值为unicode，用json解码tmp的info属性的值为dict并重新传给tmp的info属性；
* 在feed列表中添加tmp

返回feed列表

