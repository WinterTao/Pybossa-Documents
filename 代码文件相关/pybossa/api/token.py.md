# token.py

声明TokenAPI类，继承APIBase类；  

## TokenAPI类

定义_resource_name为token；  
定义oauth提供方；  

### get(self, token):
**装饰:** 设置请求频率限制，并将被装饰函数的返回值json化；  

确保_resource_name拥有read权限；   
获取所有提供方的token存为列表user_tokens；  
定义响应response；  
返回响应；  

如果报错，格式化错误信息并返回；  

### \_get_token(self, token, user_tokens):
定义token；  
如果token在user_tokens中，返回字典{token: user_tokens[token]};  
报错；  

### \_get_all_tokens(self):
定义空字典tokens；  
遍历oauth提供方的provider：  

* 用提供方作为参数创建token并赋予token；  
* 如果token存在，tokens的provider_token键值为token；  

返回token；  

### \_create_token_for(self, provider):
从当前用户的info中获得provider的信息作为token_value；  
如果token_value存在：  

* 定义token字典；  
* 如果token_value存在oauth_token_secret的键值，将其赋予token的oauth_token_value的键值；
* 返回token；  

返回None；  

### post(self):
报错；

### delete(self, oid=None):
报错；

### put(self, oid=None):
报错；




