# \__init__.py
 
定义api蓝本blueprint;  
定义错误状态对象；

## index():
**修饰：**api蓝本根地址的视图函数，并设置好请求频率限制；  
返回欢迎页文本；

## register_api(view, endpoint, url, pk='id', pk_type='int'):

定义各节点地址的视图函数和方法响应；

## new_task(project_id):
**修饰：**api新建任务地址视图函数，并设置好请求的频率限制，将返回结果json化；  
通过项目id检索新任务；

* 如果新任务是响应对象，直接返回新任务；
* 如果不是响应对象并且不为空：
    * 分配任务给用户；
    * 将任务数据字典化为data；
    * 如果：
        * data长度为0，响应空json数据
        * data长度为1，响应json化的data[0]数据
        * data长度大于1，响应json化的data数据
    
    * 其他情况，响应空的json数据；

* 如果报错，抛出格式错误

## \_retrieve_new_task(project_id):

按id获取project的数据库数据；  
如果项目不存在，报错；  
如果不允许匿名而且当前用户匿名，返回错误信息；  
如果request包含external_uid，验证project授权，如果没有授权，返回错误；  
如果request包含limit，设置limit，否则limit设置为1；  
如果limit大于100，让limit等于100；  
如果request包含offset，设置offset，否则offset等于0；  
如果request包含orderby，设置orderby，否则orderby为'id'；  
如果request有desc，设置desc，否则desc为false；  
如果当前用户是匿名的，user_id为空，否则设置为当前用户id；  
如果当前用户是匿名的，user_ip就等于匿名的request远程地址或者匿名后的127.0.0.1，否则为空；  
通过上面设置的各种参数设置新任务；  

返回新任务；

## def user_progress(project_id=None, short_name=None):

**修饰：**注册这个视图函数到四个路由地址，并设置请求频率限制，同时将返回结果json化  

如果project_id或者short_name存在：  

* 如果short_name存在，通过short_name获取project数据；
* 否则如果project_id存在，通过project_id获取project数据；
    
* 如果Project存在：
    * 获取查询属性project_id；
    * 如果当前用户匿名，设置查询属性user_ip为匿名的请求远程地址或者匿名的127.0.0.1；
    * 否则，设置查询属性user_id为当前用户id；
    * 通过查询参数设置任务运行计数器；  
    * 返回响应完成的任务；  
* 否则，返回错误代码404

否则，返回错误代码404

## auth_jwt_project(short_name):

**修饰：**对project的token页面设置视图函数，并设置请求频率限制，对返回结果json化；  

声明项目密钥；  
如果请求头中包含'Authorization'，项目密钥等于从请求头获得的密钥；  
如果项目密钥存在：  

* 通过short_name获取项目数据；
* 如果项目存在且项目密钥匹配，设置token为用项目密钥和HS256算法的jwt加密数据，并返回token；
* 否则，返回404错误；  

否则，返回403错误；

## get_disqus_sso_api():

**修饰：**设置api/disqus/sso地址下的视图函数，并设置请求频率限制，同时对返回结果json化；  

如果当前用户已被授权，获取当前用户的disqus SSO数据；  
否则，获取None的disqus SSO数据；  

如果message、timestamp、sig和pub_key都存在：

* 设置远程授权
* 返回响应远程授权和公钥

否则， 报错；

如果报错，返回错误信息 “丢失Disqus key”；  



