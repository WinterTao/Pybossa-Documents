# global_stats.py

声明GlobalStatsAPI类，继承APIBase类；

## GlobalStatsAPI

### get(self, oid=None):
获取正在等待的task的数量n_pending_tasks；  
获取总共的用户数量n_users；  
获取总项目数量n_projects；  

定义统计数据字典data；  

获取被使用的类别categories；  

以c遍历categories：

* 定义空字典datum；
* 定义datum的c的别名的键值为c类别的项目数量；  
* 向data的categories键值添加datum；  

定义空字典datum；  
datum的featured键值为featured类别的项目数量；  
向data的categories键值添加datum；

定义空字典datum；  
datum的featured键值为draft类别的项目数量；  
向data的categories键值添加datum；

返回响应；  

### post(self):
报错；


