# api_base.py

定义repos字典；
定义caching字典；  
定义错误状态；

声明APIBase类，继承MethodView类

## APIBase类

声明Hateoas对象heatoas；  
定义允许上传的类列表；  

### refresh_cache(self, cls_name, oid):

如果caching字典里有关于cls_name的值，对cls_name下的oid的缓存进行刷新；

### valid_arg(self):

遍历请求参数的key：  

* 如果key值不是api_key：
    * 获取当前函数的key的属性值

### options(self, **kwargs):
返回''；  

### get(self, oid):
**装饰:**  设置请求频率限制并将被修饰函数返回的结果json化；  

确保当前类有读取权限；  
获取以oid为参数的查询结果query；  
创建json响应json_response；  
返回响应；  

如果报错，返回格式化之后的报错信息；  

### \_create_json_response(self, query_result, oid):  

如果查询结果query_result的长度为1，并且第一个查询结果不为空，返回404错误；  

定义items空列表；  

遍历查询结果query_result：  

* 如果结果不是DomainObject：
    * 如果result的key列表里面包含'n_fays'，result等于result[0]；

* 如果result的类不是当前类：
    * 从result获取item，headline和rank；
* 否则：
    * item等于result；
    * headline和rank为空；
* 获取item加入链接后并选出特定属性的字典datum；  
* 如果headline存在，定义datum的headline键值为headline；
* 如果rank存在，定义datum的rank键值为rank；  
* 确保item有read权限；  
* 为item添加datum；  

* 如果报错禁止或未授权，如果items的长度大于1，删除第一个值；  
* 如果报其他错，报错；  

如果oid不存在：  

* 确保query_result的第一个值有read权限；
* 定义items为items的第一个值；  

返回json化之后的items；  

### \_create_dict_from_model(self,model):
将model添加链接后选择特定属性并返回；  

### \_add_hateoas_links(self, item):

将item字典化存入obj；
获取请求的参数中related键的值；  
如果related存在：

* 如果item的类名是Task：
    * 定义obj的task_runs键为一个空列表；
    * 定义obj的result键为空；  
    * 从数据库中获取符合item.id的已完成的任务列表；  
    * 从数据库中获取符合item.id的最新的result列表；  
    * 遍历task_run列表，将读取到的task字典化并添加到obj的task_run键的列表中；  
    * 遍历resluts列表，将读取到的result字典化并存入obj的result键中；
* 如果item的类名为TaskRun：
    * 从数据库获取符合item.task_id的任务列表；  
    * 从数据库获取符合item.task_id的最新的result列表；
    * 定义obj的task键为空；
    * 定义obj的result键为空；  
    * 遍历tasks列表，将读取到的task字典化并存入obj的task键中；
    * 遍历resluts列表，将读取到的result字典化并存入obj的result键中；
* 如果item的类名为Result：
    * 从数据库获取符合item.task_id的任务列表；  
    * 从数据库获取符合item.task_id的已完成的任务列表；
    * 定义obj的task_runs键为一个空列表；
    * 遍历tasks列表，将读取到的task字典化并存入obj的task键中；
    * 遍历task_run列表，将读取到的task字典化并添加到obj的task_run键的列表中；

将hateoas创建的链接存入links和link中；

如果links存在，obj的links键值等于links；  
如果link存在，obj的link键值等于link；  
返回obj；

### \_db_query(self, oid):

读取repos中当前类的info信息repo_info；  
如果oid不存在：  

* 设置limit，offset和oderby；
* 执行查询语句获得结果results；  

否则：  

* 获取repo_info的repo键值repo；  
* 获取repo_info的get键值query_func；
* 以oid为参数执行repo中的query_func函数并得到结果results；  

返回results；  

### api_context(self, all_arg, **filters):
如果当前用户已被授权，设置filters的owner_id键值为当前用户id；  
如果filters的owner_id键值存在，且all_arg等于1，删除filters的owner_id键值；  
返回filters；  

### \_filter_query(self, repo_info, limit, offset, orderby):
定义空列表filters；  
遍历请求中的参数中的键k：  

* 如果k不在指定列表内：  
    * 如果当前类为Task类，且k为'external_id'，什么也不做；  
    * 否则，执行当前类中的k函数；  
    * 定义filters的k键值为请求中的参数的k键值；  

读取repo_info的repo键值repo；  
读取请求中参数的all键值与当前filters结合得到新的filters；  
读取repo_info的filter键值query_func；  
读取自定义的filter为filter；  
获取last_id信息；  
如果请求中存在paticipated信息，获取用户的id和ip赋予filters的paticipated键值；  
获取请求中参数的fulltextsearch键值fulltextsearch；  
获取排序信息；  
执行repo中的query_func函数得到results；  
返回results；  

### \_set_limit_and_offset(self):
设置limit，offset，orderby并返回；  

### post(self):  
**装饰:** 设置请求频率限制并将被修饰函数的返回结果json化；

读取当前对象的类名cls_name；  
验证参数；  
上传request所包含的信息上传到文件并将文件信息返回给data；  
如果data不存在，读取request的信息到data；  
禁止对data中的信息使用put和post请求；  
用data作为参数创建实例inst；  
读取当前类的repo信息repo；  
读取当前类的save信息save_func；  
以inst作为参数运行repo中的save_func函数；  
更新日志；  
刷新缓存；  
返回字典化并json化之后的inst；  

如果报错，返回格式化之后的错误信息；

### \_create_instance_from_request(self, data):  

对传入的data删除其中包含的链接；  
用data包含的参数定义指定类的对象inst；  
更新inst；  
确保inst有create权限；  
验证inst；  
返回inst；  

### delete(self, oid):  
**装饰:** 设置请求频率限制并将被修饰函数的返回结果json化；

检验参数的有效性；  
删除指定对象；  
读取当前对象类名；  
刷新缓存；  
返回''和204；  

如果报错，返回格式化的错误信息；

### \_delete_instance(self, oid):

从repos获取当前对象的类名的键值中repo的键值repo；  
从repos获取当前对象的类名的键值中get的键值query_func；  
以oid做参数运行repo的query_func并获得返回值inst;  
如果不存在inst，报错NotFound；   
确保inst有delete权限；  
删除inst；  
更新日志；  
读取repos的当前对象的类名的键值的delete的键值为delet_func；  
以inst为参数运行repo下的delet_func函数；  
返回inst；   


### put(self, oid):
**装饰:** 设置请求频率限制并将被修饰函数的返回结果json化；  

检验参数；  
获取当前对象类名cls_name；  
获取repos中当前对象的类的repo键值repo；  
获取repos中当前对象的类的get键值query_func；  
获取repo中query_func以oid为参数的执行结果existing；

如果existing不存在，报NotFound错误；  

确保existing对象有update权限；  
通过请求获取上传的文件的数据；  
跟新数据并获取更新后的数据inst；  

刷新缓存；  
返回响应；  

如果报错：

* 格式化报错信息并返回；

### \_update_instance(self, existing, repo, repos, new_upload=None):
定义空字典data()；  
如果new_upload不存在：  

* 将请求的data进行json化并存入data；
* 禁止在post和put请求中使用data中的属性；  
* 删除data中的链接

否则，禁止在post和put请求中使用request的form中的属性；

通过data设置当前对象；  
定义old为通过字典化的existing值设置的当前类的对象；  

遍历data中的key：

* 设置existing的媒体文件链接为新上传字典的媒体文件链接；  
* 设置existing的container为新上传字典的info字典的container；
* 设置existing的file_name为新上传的字典的info字典的file_name；

通过existing对old进行更新；  
设置update_func为当前类在repo字典中的update键值；  
验证existing对象；  
以existing对象为参数运行repo类的upload_func；  
更新log；  
返回existing对象；


### \_update_object(self, data_dict): 
pass；

### \_update_attribute(self, new, old):
等待重写；

### \_select_attributes(self, item_data):
返回item_data；

### \_custom_filter(self, query):
返回query；  

### \_validate_instance(self, instance):
pass;

### \_log_changes(self, old_obj, new_obj):
pass;

### \_forbidden_attributes(self, data):
pass;

### \_file_upload(self, data):
获取类名cls_name；  
设定内容类型；  
如果内容类型在请求的标头列表中，且类在运行上传的类列表中：

* 定义空字典tmp；
* 遍历请求的form中的key：  
    * 定义tmp的key键值等于请求的form中的key键值；

* 如果当前变量时Announcement类的一个实例：
    * 确保当前变量有create权限；  
    * 获取上传方法；  
    * 如果request中并没有请求file，报属性错误；
    * 读取request中的file忠的'file'键值_file; 
    * 定义container为user_加当前用户的id；
* 否则：
    * 确保当前类有create权限；  
    * 从数据库获取指定项目的数据；
    * 设置上传方法；  
    * 如果request中并没有请求file，报属性错误；
    * 读取request中的file忠的'file'键值_file; 
    * 如果当前用户为管理员，定义container为user_加项目拥有者的id
    * 否则，定义container为user_加当前用户的id；

* 用上传器上传文件；
* 获取文件地址；
* 设置tmp的媒体地址键值为获取到的文件地址；  
* 如果tmp的info键值为空，设置info键值为空字典；
* 定义tmp的info键值的container键值为container；
* 定义tmp的info键值的file_name键值为_file的filename；  
* 返回tmp；

否则，返回None；

### \_file_delete(self, request, obj):
获取类名cls_name;
如果类名在允许上传的类列表里面：

* 获取obj的info中的keys；
* 如果'file_name'在keys列表中，且'container'在keys列表中： 
    * 确保obj有删除权限；  
    * 通过上传器删除相关文件；

    


