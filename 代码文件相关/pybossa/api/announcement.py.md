# announcement.py

声明AnnouncementAPI类，继承APIBase类；  

## AnnouncementAPI类：

定义reserved_keys;  
定义指向类__class__;  

## \_forbidden_attributes(self, data):

遍历data中的key：  

* 如果key出现在reserved_keys中，报错；

## \_update_object(self, obj):

如果当前用户不是匿名用户，obj的用户id等于当前用户的id；  

