# project.py

定义Auditlogger对象auditlogger；  
声明ProjectAPI类；

## ProjectAPI类

定义指向类Project；  
定义reserved_keys列表；  
定义private_keys列表；  

### \_create_instance_from_request(self, data):

以data为参数创建指向的类__class__的实例inst；  
获取默认类别；  
定义inst的类别id为默认类别的id；  
返回inst；  

### \_update_object(self, obj):

如果当前用户输出匿名：  

* obj的owner_id等于当前用户id；  
* 获取拥有者列表；  
* 如果当前用户id不在拥有者列表中，将其添加进去；  
* 更新obj的拥有着列表；  

### \_validate_instance(self, project):

如果项目的别名存在，且项目别名已经注册到project蓝本中，报错ValueError；  

### \_log_changes(self, old_project, new_project):
更新log；  
 
### \_forbidden_attributes(self, data):
遍历data中的key：  

* 如果key出现在reserved_keys中：
    * 如果key为'published'， 报错；
    * 报错；  

### \_filter_private_data(self, data):
深复制data的数据到tmp；  
读取Project类的public_attributes到public；  
为public列表添加link和links；  
遍历tmp的key，如果key不在public中，从tmp中删除；  
遍历tmp的info键值的key，如果key不在public的info_key列表中，删除；  
返回tmp；  
 
### \_select_attributes(self, data):
如果当前用户匿名，返回private_data；  
如果当前用户已被授权，且当前用户是owner或管理员，返回data；
否则，返回private_data;  


