# vmcp.py

声明VmcpAPI类，继承APIBase类

## VmcpAPI：

### get(self, oid=None):
**装饰:** 设置请求频率限制，并将被修饰函数的返回值json化；  

如果当前应用没有关于'VMCP_KEY'的设置：

* 定义错误信息；
* 返回报错响应；  

定义pkey；

如果pkey文件不存在：

* 定义错误信息；
* 返回报错响应；  

如果请求中没有关于cvm_salt的参数：  

* 定义错误信息；
* 返回报错响应；  

定义salt为请求中关于cvm_salt的参数的值；  
定义data为request的参数的浅复制；  
定义signed_data；

返回signed_data的响应；

### \_format_error(self, status_code=None, message=None):

以字典形式返回错误信息；  

### post(self):
**装饰:** 设置请求频率限制；

报错；  

