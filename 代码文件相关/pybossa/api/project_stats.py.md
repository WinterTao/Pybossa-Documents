# project_stas.py

声明ProjectStatsAPI类，继承APIBase类；  

## ProjectStatsAPI类：
 
定义指向类__class__; 

### \_select_attributes(self, stats_data):
如果request的参数的full键值为False：  

* 复制stats_data的值到tmp；  
* 从tmp的info键值中删除小时统计；  
* 从tmp的info键值中删除日期统计；
* 从tmp的info键值中删除用户统计；
* 返回tmp；  

返回 stats_data；


