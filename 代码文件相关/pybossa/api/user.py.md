# user.py

声明UserAPI类，继承APIBase类；  

## UserAPI类：

定义指向类__class__；  

定义public——attributes元组；  
定义allowed_attributes元组；

### \_select_attributes(self, user_data):

如果当前用户已被授权，且当前用户是管理员或者当前用户的id等于user_data的id：  

* 读取user_data中可以公开的attributes的json形式到tmp；  
* 设置tmp的id，email_addr，info为user_data的id，email_addr，info；
* 返回tmp；

否则：

* 获取user是否是private，存入privacy；
* 遍历user_data的键attribute，从user_data中删除其中私有的attributes；
* 返回user_data；  

### \_remove_attribute_if_private(self, attribute, user_data, privacy):
如果attribute是私有的，从user_date删除attribute的键值；  

### \_is_attribute_private(self, attribute, privacy):

返回attribute不在allowed_attributes中，或者privacy为真且attribute不在public_attributes中；  

### \_is_user_private(self, user):

返回requester不是管理员而且user的privacy_mode为真；  

### \_is_requester_admin(self):

返回当前用户已被授权而且是管理员；  

### \_custom_filter(self, filters):
如果request中有private attributes而且requester不是管理员，设置filter的privacy_mode为False；  
返回filter；

### \_private_attributes_in_request(self):
遍历请求的参数中包含的键attribute：  

* 如果attribute在allowed_attributes中，而且不在public_attributes中，返回True；  

返回False；

### post(self):
**装饰:** 设置请求响应频率限制，并将被修饰函数的返回值json化；  

报错；  
将报错信息格式化并返回；

### delete(self, oid=None):
**装饰:** 设置请求响应频率限制，并将被修饰函数的返回值json化；  

报错；  
将报错信息格式化并返回；

