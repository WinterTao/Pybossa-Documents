# favorites.py

定义错误中状态；  
声明FavoritesAPI类，继承APIBase类；

## FavoritesAPI类：  

定义指向类__class__；

## get(self, oid):
**装饰:** 设定请求频率限制，并将被装饰函数的返回结果json化；  

如果当前用户匿名，返回404错误；  
获取当前用户id，uid；  
设定limit，offset，orderby；  
从request中获取last_id；  
打印last_id；  
从request中获取排序设置；  
从数据库获取当前用户最喜欢的任务数据tasks；  
创建json形式的响应；  
返回响应；  

如果报错，返回格式化后的报错信息；  

## post(self):
**装饰:** 设定请求频率限制，并将被装饰函数的返回结果json化； 

验证参数；  
从请求中获取data；  
如果data的keys的长度不等于1，或者data的keys中不包含task_id，报错；
如果当前用户匿名，报未授权错误；  

读取当前用户id为uid；  
从数据库读取当前用户的favorite任务列表；  
如果任务列表长度等于1，读取任务列表第一个任务task；  
如果任务列表长度为0：

* 从数据库获取指定任务task；  
* 如果task不存在，报错；
* 添加uid到fav_user_ids列表；  
* 在数据库更新task数据；  
* 更新log；  

返回响应；  
如果报错，返回格式化后的错误信息；  

## delete(self, oid):   
**装饰:** 设定请求频率限制，并将被装饰函数的返回结果json化；  

如果当前用户不存在，返回401错误；  

读取当前用户id为uid；  
从数据库获取favorite的tasks数据；  
如果tasks列表为空，报错；  
如果tasks列表长度为1，task为tasks的第一个值；  

获取最喜欢task的列表中当前用户的index；  
通过index将当前用户从task的最喜欢列表里面删除；  
在数据库更新task；  
返回响应；  

如果报错，返回格式化后的错误信息；  

## put(self, oid):  
报错；


