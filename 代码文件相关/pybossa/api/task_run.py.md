# task_run.py

声明TaskRunAPI类，继承APIBase类；  

## TaskRunAPI类：

定义reserved_keys;  
定义指向类__class__; 

### \_update_object(self, taskrun):

从数据库获取指定的已完成的任务数据task；  
定义guard；  

验证task和taskrun的信息是否匹配；  
确保task被标记在guard中；  
为taskrun添加用户信息；  
为taskrun添加时间戳；  

### \_forbidden_attributes(self, data):
遍历data中的key：  

* 如果key出现在reserved_keys中，报错；
 
### \_validate_project_and_task(self, taskrun, task):

如果task不存在，报错；  
如果task的项目id不等于taskrun的项目id，报错；

如果taskrun的external_uid存在：

* 验证project的授权信息并将结果存入resp；
* 如果resp是一个响应，读取响应的描述并报错；

### \_ensure_task_was_requested(self, task, guard):

如果task不被标记个用户，报错；  

### \_add_user_info(self, taskrun):
如果taskrun的external_id为None：  

* 如果当前用户匿名：  
    * taskrun的用户ip等于加密的ip；  
* 否则，taskrun的用户id等于当前用户的id；  

否则，taskrun没有用户id和用户ip；  

### \_add_created_timestamp(self, taskrun, task, guard):
为已完成的任务创建时间戳；  
将任务从guard中移除；  

