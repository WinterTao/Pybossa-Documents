# heteoas.py

声明Hateoas类；  

## Hateoas类

### link(self, rel, title, href):

返回html语句hateoas链接；  

### create_link(self, item_id, title, rel='self'):

获取.api_title的链接地址；  
返回hateoas链接；  

### create_links(self, item):

获取item的类名cls；
创建链接列表links；
  
如果是result类：
 
* 创建标题为result的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 如果item的任务id存在，创建关于任务为标题的链接并添加进链接列表；
* 返回链接列表和link；  

否则如果是taskrun类：

* 创建标题为taskrun的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 如果item的任务id存在，创建关于任务为标题的链接并添加进链接列表；
* 返回链接列表和link；  

否则如果是task类：

* 创建标题为taskrun的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 返回链接列表和link；
    
否则如果是category类：

* 返回Nonw和标题为category的链接；  

否则如果是project类：

* 创建标题为project的链接link；
* 如果item的类别id存在，创建关于类别为标题的链接并添加进链接列表；
* 返回链接列表和link；

否则如果是user类：

* 创建标题为project的链接link；
* 返回None和link；

否则如果是blogpost类：

* 创建标题为blogpost的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 返回链接列表和link；

否则如果是announcement类：

* 返回None和announcement为标题的链接；  

否则如果是helpingmaterial类：

* 创建标题为helpingmaterial的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 返回链接列表和link；

否则如果是projectstats类：  

* 创建标题为projectstats的链接link；
* 如果item的项目id存在，创建关于项目为标题的链接并添加进链接列表；
* 返回链接列表和link；  

否则，返回False；

## remove_links(self, item):  

如果item存在link键，删除link键值；  
如果item存在links键，删除links键值；  

返回item；

