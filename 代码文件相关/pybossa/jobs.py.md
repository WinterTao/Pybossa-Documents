# jobs.py

## schedule_job(function, scheduler):

从scheduler获取job队列scheduled_jobs;  
使用scheduler通过function定义新的job；  
检查队列中是否已经存在想要添加的job，如果存在则取消掉新添加的job并返回提示信息；  
返回信息提信息说明新的job已添加；  

## get_quarterly_date(now):

如果now不是期望的日期类型，报类型错误；  
将日期转化为季度；  

## enqueue_job(job):
将job载入sentinel的master服务器队列中；  

## enqueue_periodic_jobs(queue_name):

获得周期性job的列表；  
连接job队列；  
编列周期性job的列表：  

* 如果job的队列名与指定队列名相同：  
    * 将job排入队列

返回信息；  

## get_periodic_jobs(queue):  
获取默认jobs；  
压缩项目jobs；  
获取项目jobs；    
获取自动导入器jobs；  
获取未激活用户jobs；  
获取没参与的用户jobs；  
获取dashboard的jobs；  
获取leaderboard的jobs；  
获取每周统计更新jobs；  
获取维护jobs；  
将得到的jobs存成列表；  
返回_all中的所有元素中包含的所有job中关于queue的值等于指定queue的job所组成的一个生成器；  

## get_default_jobs():

设置超时为当前应用设置的超时时间；  
从设置中获取未发布的应用；  
yield返回一些默认需要的jobs；

## get_maintenance_jobs():

从当前应用设置中获得超时时间；  
yield返回维护job；  

## get_export_task_jobs(queue):
定义feature_handler；  
读取当前应用缓存超时时间；  
如果更新导出是只属于高级用户的：  

* 如果队列级别是"high"，获取高级用户发布的项目；
* 否则，获取非高级用户发布的项目；

否则，获取所有已发布的项目；  
遍历获取的项目：  

* 获取项目id；  
* 配置job；  
* yield返回job；

## project_export(_id):

通过传入id获取项目数据；  
如果项目存在：  

* 对项目用json导出器打包导出zip文件；
* 对项目用cvs导出器打包导出zip文件；

## get_project_jobs(queue):

获取当前应用缓存超时时间；  
如果queue的级别是'super'，获取高级用户发布的项目列表；
否则如果queue的级别是'high'，获取非高级用户发布的项目列表；  
否则，项目列表为空；

遍历项目列表：

* 获取项目ID；
* 获取项目的short_name；
* 配置job；
* yield返回job；  

## create_dict_jobs(data, function, timeout, queue='low'):

遍历data:  

* 配置job；
* yield返回job；

## get_inactive_users_jobs(queue='quaterly'):

查询最后一次参与任务的时间为一年前到三个月前这段时间的用户；  
获取当前应用缓存超时时间；   
逐行读取查询到的用户数据：  

* 从User表中获取用户数据；  
* 如果用户已经订阅邮件提醒：  
    * 将邮件提醒加入job队列；
    * yield返回job；

## get_dashboard_jobs(queue='low'): 
获取当前应用缓存超时设置；  
yield返回关于dashboard的各种job；  

## get_leaderboard_jobs(queue='super'):
获取当前应用的缓存超时设置；  
获取当前应用的关于leaderboards的设置；

如果leaderboard的设置存在：  

* 遍历leaderboards：  
    * yield返回leaderboard的job 
* 返回leaderboard的job；

## get_non_contributors_users_jobs(queue='quaterly'):
查询没有参与任务的用户id；  
获取当前应用缓存超时时间；  
遍历获取到的用户id数据：

* 通过用户ID获取用户数据；  
* 如果用户已经订阅邮件通知系统：  
    * 配置邮件job；
    * yield返回job；
    
## get_autoimport_jobs(queue='low'):
配置feature_handler；  
获取当前应用缓存超时时间；  
如果autoimporter功能只针对高级用户，获取高级用户发布的项目列表；  
否则，获取所有用户发布的项目列表；  
遍历项目列表： 
 
* 通过项目ID从数据库中获得项目数据；
* 如果项目拥有自动导入器：  
    * 配置导入项目的job；
    * yield返回job；

## get_project_stats(_id, short_name):
**装饰@with_cache_disabled:** 暂时关闭缓存，执行完被修饰函数后再恢复原来的设置；  

执行各种统计；
返回True；

## warm_cache():
**装饰@with_cache_disabled:** 暂时关闭缓存，执行完被修饰函数后再恢复原来的设置；  

创建应用；
定义被缓存的项目列表；  

```text
内部函数warm_project(_id, short_name, featured=False):
如果传入_id不在被缓存的项目列表中：  

* 更新对_id的项目统计；  
* 将_id加入已缓存的项目ID列表；  
```

获取已缓存的排名前四的项目数据；  
遍历获得的项目列表，对项目进行缓存；

获取已缓存的前三页项目数据；  
遍历获得的项目列表，对项目进行缓存；  

获得已使用的已缓存的类别数据；  
遍历获得的类别列表：  

* 获得当前类别下前三页的项目数据列表；  
* 遍历获得的项目列表，对项目进行缓存；

获得已缓存的符合应用leaderboard设置的用户列表；  
遍历获得的用户列表：  

* 打印用户数据；  
* 缓存用户的各项数据；

返回True；  

## get_non_updated_projects():  

查询三个月内没更新的未完成的项目id；  
读取获得的id并在数据库中通过id获取项目数据列表；  
返回获得的项目数据列表；  

## warn_old_project_owners():

获取三个月内没有更新的项目列表；

连接邮箱系统：

* 遍历获得的项目列表：
    * 配置邮件消息数据；
    * 发送邮件消息；
    * 设置项目的联系状态为已联系；
    * 设置项目的发布状态为未发布；
    * 清除项目缓存；
    * 数据库中更新项目；

返回True；

## send_mail(message_dict):
配置要发送的邮件消息；  
发送邮件消息；

## import_tasks(project_id, from_auto=False, **form_data):
从数据库获取项目数据；  
导入创建任务列表report；  
如果from_auto为True:  

* 设置form_data的最新元数据为report的元数据；
* 设置自动导入器；
* 将项目添加进数据库；  

配置邮件消息；  
发送邮件；  
返回邮件消息信息；  

## webhook(url, payload=None, oid=None):

获取指定项目；  
设置标头；  
如果准则oid，通过oid获得webhook；  
否则，通过项目ID和payload数据获得webhook；  

如果存在url：  

* 设置post请求响应；  
* 获取网页正文作为webhook的响应；  
* 设置webhook的状态码为response的状态码；  

否则，报连接错误；  

如果oid存在：  

* 数据库更新webhook；
* 通过oid获取webhook；  

否则，数据库存入webhook；  

如果遇到连接错误：  

* webhook响应错误代码；  
* 数据库存入webhook；  

finally:   

* 如果项目已发布，且webhook的响应状态码不等于200，且当前程序的ADMINS设置为真：  
    * 配置邮件消息；  
    * 发送邮件；  

如果开启了SSE，发布频道消息；  
返回webhook；  

## notify_blog_users(blog_id, project_id, queue='high'):

通过博客ID从数据库获取博客数据；  
配置高级用户的feature_handler；  
读取通知博客升级特征是否是高级特征；  
设置当前程序缓存超时；  

如果博客的项目已经特征化，或者博客项目的拥有者是高级用户或者通知博客更新并不是高级特征：  

* 查询已订阅邮件通知的完成过制定项目的任务的用户；  
* 逐行读取获得的用户数据：  
    * 配置邮件消息；
    * 将邮件消息保存为job；  
    * 将job传入job队列；  
    * 用户数统计加一；  

返回'多少个用户被邮件通知'的消息；  

## get_weekly_stats_update_projects():
配置高级用户的feature_handler；  
读取“项目周报”是否为高级功能；  
根据上面的结果，设置仅限高级用户的sql语句；  
读取“每周更新统计时间”设置为发送邮件星期；  
读取今天的星期；  
读取当前应用缓存超时设置；  

如果今天的星期和发送邮件星期相同：

* 查询包含未完成任务且拥有者订阅了邮件提醒的项目的ID，以及已经标注了可以用高级功能的项目id；  
* 逐行读取查询到的数据：
    * 将结果存为job；
    * yield返回job；

## send_weekly_stats_project(project_id):
从数据库获取指定的项目；  
如果用户没有订阅邮件消息，返回消息；  
更新指定项目的统计信息；  
获取指定项目的各项信息；  

配置邮件消息；  
将邮件消息存入job；  
将job加入队列；

## news():

定义urls；  
定义score为0；  
定义notify为False；  

如果当前应用有设置新闻链接，将连接加入urls；  
遍历urls：  

* 解析地址得到数据d；  
* 按score获取消息；  
* 如果d存在块而且获取的消息长度不为0，或者获取的第一条消息的updated属性的值不等于解析地址d的第一个块的updated属性值：
    * 将d的第一个块的数据加入sentinel主服务器；  
    * 设置notify为False；  
* score+=1；

如果notify为True，推送消息给管理员；

## check_failed():

获取失败的队列fq；  
获取fq中的job的ID；  
计算job的数量count；  

读取重试失败jobs次数的设置FAILED_JOBS_RETRIES；  

遍历jobs的id：

* 设置KEY；
* 获得job_id指定的job；  
* 如果sentinel的从服务器存在KEY，主服务器对KEY进行加一操作；  
* 否则：
    * 设置ttl为'FAILED_JOBS_MAILS'的设置\*24\*60\*60
    * 为主服务器加入KEY-ttl对，并设置失效时间为1

* 如果sentinel从服务器获得的key的值取整后小于FAILED_JOBS_RETRIES，重新将job加入队列；
* 否则：
    * 设置KEY
    * 如果从服务器不存在KEY的值而且当前程序有管理员：
        * 配置邮件消息；
        * 发送邮件消息；
        * 设置ttl为'FAILED_JOBS_MAILS'的设置\*24\*60\*60
        * 为主服务器加入KEY-ttl对，并设置失效时间为True

返回处理信息；  

## push_notification(project_id, **kwargs):
获取项目数据；
如果项目的info里面有关于onesignal的信息：

* 获取onesignal程序的id；
* 获取onesignal程序的key；
* 创建程序对象并发送消息；
* 设置过滤器；  
* 返回客户端推送消息后的返回信息；

