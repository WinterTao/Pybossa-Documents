# core.py

**代码解释：**

## create_app(run_as_server=True)
获取一个Flask对象app；  
对app进行初始设置；  
打包app的一些配置文件如CSS，js等；  
设置缓存超时；  
设置请求频率限制；  
设置主题；  
设置上传器；  
设置错误邮箱；  
设置日志；  
设置登录管理；  
设置babel；  
设置Markdown；  
设置数据库；  
设置仓库接口；  
设置导出器；  
设置强密码；  
初始化邮箱设置，用于发送邮件；  
初始化sentinel设置；  
初始化signer设置；  
如果'SENTRY_DSN'为true，设置sentry服务器；  
如果作为服务器运行，设置job队列；  
设置蓝本；  
设置钩子；  
设置error控制器；  
设置LDAP；  
设置额外服务；  
设置jinja模板；  
设置csrf保护；  
设置debug工具栏；  
设置jinja2模板过滤器；  
设置消息推送；  
设置SSE服务；  
设置json编码器；  
设置跨源资源共享；  
设置分析器；  
初始化插件管理器；  
安装插件；  
设置用户偏好设置和其他设置元数据；  
设置ip地址隐藏器；  

返回完成设置的app；  

## configure_app(app):
从pybossa/defualt_settings对象载入设置;  
尝试从环境变量'PYBOSSA_SETTINGS'载入设置；  
如果环境变量'PYBOSSA_SETTINGS'不存在，尝试从settings_local.py中载入设置；  
尝试设置测试用的数据库地址；  
在丢失Master结点的时候绑定Slave；  
设置url后面是否需要严格加斜杠；  

## setup_json_serializer(app):
设置json编码器；  

## set_cors(app):
初始化跨院资源共享；  

## setup_sse(app):
如果开启SSE，提示需要开启异步模式；  

## setup_theme(app):
设置主题；  

## setup_uploader(app):
定义全局变量uploader；  
如果上传方法为本地上传，载入并初始化本地上传器；  
如果上传方法为云空间上传，载入并初始化云空间上传器，同时添加错误控制；  

## setup_exporter(app):
设置全局变量csv_exporter和json_exporter;  
载入csv导出器和json导出器；  

## setup_markdown(app):
初始化misaka，以使程序可以再模板中使用markdown语句；  

## setup_db(app):
初始化数据库并设置从数据库；  
如果数据库的session和从数据库的session不一致，在应用上下文结束销毁时，执行和主数据库一样的操作；  

## setup_repositories(app):
设置各种内容的数据库仓库接口；  

## setup_error_email(app):
设置错误邮箱，如果没有debug而且有设置管理员，设置程序出现错误时向管理员发送邮件；  

## setup_logging(app):
设置日志文件路径、格式和记录级别；  

## setup_login_manager(app):
设置用户登录管理器；

## setup_babel(app):
初始化babel用来进行时间和地址格式的设置；

## setup_blueprints(app):
设置各蓝本的地址前缀；

## setup_external_services(app):
设置各种额外服务，如各种登录方式，各种导入方式等；

## setup_jinja(app):
设置其他页面的jinja模板；

## setup_error_handlers(app):
设置各种错误代码的响应函数和模板；

## setup_hooks(app):
设置请求前、请求后、执行中和csrf攻击报错的钩子

## setup_jinja2_filters(app):
设置jinja2模板中的自定义过滤器

## setup_csrf_protection(app):
设置csrf保护

## setup_ratelimits(app):
声明全局变量ratelimits;  
从设置中读取频率限制的参数设置；

## setup_cache_timeouts(app):
声明全局变量timeouts;  
读取超时参数设置并定义各种状态下的缓存超时参数；

## setup_scheduled_jobs(app):
通过使用rq_scheduler规划jobs队列；

## setup_newsletter(app):
设置mailchimp的订阅推送；

## setup_assets(app):
设置对静态CSS、js等文件的打包；

## setup_strong_password(app):
声明全局变量enable_strong_password;  
读取设置是否开启强密码；

## setup_ldap(app):
尝试设置LDAP服务器；

## setup_profiler(app):
尝试设置flask分析器；

## setup_upref_mdata(app):
声明全局变量 upref_mdata_choices;  
读取用户的账户设置元数据；


