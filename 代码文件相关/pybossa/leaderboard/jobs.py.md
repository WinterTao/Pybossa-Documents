# jobs.py

## def leaderboard(info=None):

定义物化视图materialized_view；  
定义物化视图索引materialized_view_idx；  

如果info存在：  

* 重新定义materialized_view;  
* 重新定义materialized_view_idx;

如果数据库中存在物化视图materialized_view，刷新物化视图并返回刷新信息；  

否则：

* 创建物化视图并提交；
* 为物化视图创建索引；
* 返回创建信息；  



