# data.py

定义User对象u；

## get_leaderboard(top_users=20, user_id=None, window=0, info=None):

定义物化视图名字materialized_view;

查询排名在前top_users位的用户；  
将查询到的数据json化之后存入top_users；  

如果存在user_id：  

* 查询指定排名中的指定用户；  
* 读取查询到的用户user；  
* 如果user存在而且window不等于0：  
    * 查询指定排名区域的用户，最高为user的排名减去window，最低位user的排名加上window；  
    * 读取查询到的数据并添加到top_users后面；  
* 否则，如果user存在，user添加到top_users后面；  
* 返回top_users；  

返回top_users；

## format_user(user):

对user数据json化并返回；


