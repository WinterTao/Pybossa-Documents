# \__init.py__

声明Repository类，继承object；

## Repository类 

### \__init__(self, db, language='english'):
初始化定义数据库为db；  
初始化语言；  

### generate_query_from_keywords(self, model, fulltextsearch=None, **kwargs):
遍历kwargs字典中的项目，选出key不为info、fav_user_ids、created和project_id的，对比value值和model中关于key的描述是否一致，将对比结果组成列表存入clauses；  

定义空列表queries、headlines、order_by_rank和or_clauses；

如果kwargs的键列表中有'info'：

* 获得queries，headlines，order_by_ranks；
* 将queries添加到clauses中；

如果kwargs的键包含created：

* 定义like_query为kwargs的created键值加%；  
* clauses添加model关于created的描述是否是以kwargs的created键值开头的布尔值；

如果kwargs的键包含project_id：

* 定义tmp为字符串化的kwargs的project_id的键值；
* 从tmp中提取出数字作为project_ids列表
* 遍历project_ids:
    * or_clauses添加model关于project_id的描述是否与project_id匹配的布尔值；

定义all_clauses为对所有clauses取and运算，对所有or_clauses区or运算，然后对两个运算结果取and运算；

返回 （all_clauses,), queries，headlines，order_by_ranks；

### handle_info_json(self, model, info, fulltextsearch=None):

定义空列表clauses、headlines和order_by_ranks；

如果info存在而且info中含有'::':

* 按|分割info获得列表pairs；
* 遍历pairs中的pair：
    * 如果pair不为'':
        * 按::分割获得键k值v；  
        * 如果fulltextsearch等于1：  
            * 定义vector为model关于info的描述的文本；  
            * 将vector排序去重，再与v匹配，将匹配结果的布尔值传给clause
            * 将clause添加到clauses中；
                * 如果headline的长度是0：
                    * 获取headline和order_by_rank；
        * 否则，clauses添加“model关于info的描述的k键值是否与v匹配”的布尔值

否则：

* 如果info是字典：
    * 添加model关于info的描述是否与info相符的布尔值到clauses；
* 如果info是字符串或unicode：
    * 将info字典化
    * 如果得到的info是int或者float，将info转换为字符串；
    * 如果报错，将info转换为字符串；
    * clause添加model关于info的描述是否包含info的布尔值

返回 clauses，headlines，order_by_ranks；  

### create_context(self, filters, fulltextsearch, model):
声明owner_id，query和participated；  
如果filters有owner_id的键值：

* 定义owner_id为filters的owner_id键值；
* 删除filters中owner_id的键值；

如果filters有participated的键值，且filters有external_uid的键值，删除filters中external_id的键值；  

如果filters中有participated的键值：

* 定义participated为filters的participated键值；
* 删除filters中participated的键值；

获取query_args，queries，headlines，orders；

如果model不在Announcement或者ProjectStats中，并且owner_id存在:

* 定义子查询subquery；
* 根据不同情况定义查询；

否则，直接定义查询；

如果participated为真，而且model是Task：

* 根据participated的不同情况定义查询语句；

如果headline长度大于0，query添加headline[0]列；
如果orders长度大于0，query添加orders[0]列，并按降序排序；

返回查询语句query；

### \_set_orderby_desc(self, query, model, limit, last_id, offset, descending, orderby):

设置排序和升降序方式；

### \_filter_by(self, model, limit=None, offset=0, yielded=False, last_id=None, fulltextsearch=None, desc=False, orderby='id', **filters):

获取查询语句；

如果last_id存在，定义filter和排序方式
否则，只定义排序方式

如果yielded为真：

* 设置limit为limit或者1；
* query添加yield_per为limit；

返回query.all()

***

import各种仓库接口类；
断言它们存在；


