# Pybossa Documents

这个项目仅用于保存Pybossa一些相关的文档，用于对官方文档的一些拓展和补充，以帮助新手了解和熟悉Pybossa

* **部署相关**：
    * [Mac下部署Pybossa](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E9%85%8D%E7%BD%AE%E7%9B%B8%E5%85%B3/Mac%E4%B8%8B%E9%83%A8%E7%BD%B2Pybossa.md)
    * [使用uwsgi和nginx部署Pybossa](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E9%85%8D%E7%BD%AE%E7%9B%B8%E5%85%B3/%E5%9C%A8nginx%E5%92%8Cuwsgi%E4%B8%8A%E9%83%A8%E7%BD%B2pybossa.md)

* **一些概念**:
    * [Pybossa中用到的各种组件](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E4%B8%80%E4%BA%9B%E6%A6%82%E5%BF%B5/Pybossa%E4%B8%AD%E7%94%A8%E5%88%B0%E7%9A%84%E5%90%84%E7%A7%8D%E7%BB%84%E4%BB%B6.md)
    * [ORM (Object Relational Mapping)](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E4%B8%80%E4%BA%9B%E6%A6%82%E5%BF%B5/ORM(Object%20Relational%20Mapping).md)
    * [SSE (Server Sent Events)](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E4%B8%80%E4%BA%9B%E6%A6%82%E5%BF%B5/SSE%20(Server%20Sent%20Event).md)
    * [LDAP (Lightweight Directory Access Protocol)](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E4%B8%80%E4%BA%9B%E6%A6%82%E5%BF%B5/LDAP.md)
    * [Docker](https://gitlab.com/WinterTao/Pybossa-Documents/blob/master/%E4%B8%80%E4%BA%9B%E6%A6%82%E5%BF%B5/Docker.md)