# Pybossa中用到的各种组件
## PostgreSQL
PostgreSQL是比较常见的几种数据库管理系统之一，性能优异，稳定性强，而且有很多可用的窗口函数。

## Redis
是一种高效的支持多种数据类型的高性能Key-Value数据结构储存系统，Flask中使用Redis做缓存数据管理。Redis会把数据读入内存中以提高存取效率，支持超过100K+每秒的读写频率，还支持key过期等特性。极适合做缓存。在Pybossa中，Redis不仅用来做缓存和加速，也被用于限制API请求的使用率。

## sentinel
sentinel可以用于对redis服务器进行监控，判断其是否下线，并进行自动故障迁移，提升其可用性。是redis服务器稳定运行的一个保障组件。

## SQLAlchemy
SQLAlchemy是Python中的一个包，主要用于连接数据库和Python代码，使程序可以通过Python代码直接对数据库进行管理

## Alembic
Alembic是Python中的一个包，是一个数据库版本管理工具

## RQ
RQ是Python中的一个包，是一个简单的分布式异步作业管理系统。你可以把想要异步执行的操作都写成job函数，然后构建一个queue并把这些job给载入进去。之后queue会将这些job分配给RQ中的独立进程worker来执行并返回执行结果。

## Flask
Flask是一个Python编写的web微框架，帮助开发者运用python语言快速实现一个网站或者web服务

### Blueprint 蓝本
Flask里的blueprint(蓝本)和程序类似，可以定义路由，但是蓝本定义的路由处于休眠状态，在被注册到程序上时才会被唤醒。

## LDAP
目录是为查询、浏览和搜索而优化的专业分布式数据库，呈树状结构。和关系数据库不同的是，目录数据库拥有更优秀的读性能，但是写性能很差，也没有事务处理、回滚等功能，不适合储存和修改繁杂的数据。一般用来做查询，适合储存像以下这种信息：

* 企业员工信息，如姓名、电话、邮箱等；
* 公用证书和安全密钥；
* 公司的物理设备信息，如服务器，它的IP地址、存放位置、厂商、购买时间等；

LDAP则是一种轻量级的目录数据库访问协议，其特点有：

* LDAP的结构用树来表示，而不是用表格。正因为这样，就不能用SQL语句了
* LDAP可以很快地得到查询结果，不过在写方面，就慢得多
* LDAP提供了静态数据的快速查询方式
* Client/server模型，Server 用于存储数据，Client提供操作目录信息树的工具
* 这些工具可以将数据库的内容以文本格式（LDAP 数据交换格式，LDIF）呈现在您的面前
* LDAP是一种开放Internet标准，LDAP协议是跨平台的Interent协议

LDAP在pybossa中被用于用户身份验证。

## SSE
SSE(Server Sent Events)是一种服务器主动向浏览器推送信息的技术，和WebSocket在服务器和客户端之间建立双向实时通信不同的是，SSE仅支持从服务器到客户端的单向实时通信。另外SSE仅支持以文本的形式传输数据。不过SSE的实现更为简单，可以完全复用现有的HTTP协议，而不用像WebSocket有一套独立的标准。浏览器支持方面目前SSE没有WebSocket全面且完全不支持IE和Edge,在使用时需要根据浏览器类型判断是否开启。

在Pybossa中，SSE需要在异步模式下运行，为此需要对uwsgi和nginx进行配置

## uwsgi
uwsgi是Python中的一个包。它是一种接口，定义了web服务器和web应用之间的接口规范

## nginx
nginx是个高性能的Web和反向代理服务器

## supervisor
supervisor是一个进程管理工具，被管理的进程被作为supervisor的子进程来启动，只需要将可执行文件的路径添加进配置文件就行了。如果子进程异常终端，父进程可以直接准确获取到子进程地异常终端信息，如果在配置文件中有设置autostart=true，子进程将被自动重启。



