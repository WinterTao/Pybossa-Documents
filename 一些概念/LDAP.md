# LDAP (Lightweight Directory Access Protocol)
[参考：LDAP服务器的概念和原理简单介绍](https://www.cnblogs.com/yjd_hycf_space/p/7994597.html)

***

目录是为查询、浏览和搜索而优化的专业分布式数据库，呈树状结构。和关系数据库不同的是，目录数据库拥有更优秀的读性能，但是写性能很差，也没有事务处理、回滚等功能，不适合储存和修改繁杂的数据。一般用来做查询，适合储存像以下这种信息：

* 企业员工信息，如姓名、电话、邮箱等；
* 公用证书和安全密钥；
* 公司的物理设备信息，如服务器，它的IP地址、存放位置、厂商、购买时间等；

LDAP则是一种轻量级的目录数据库访问协议，其特点有：

* LDAP的结构用树来表示，而不是用表格。正因为这样，就不能用SQL语句了
* LDAP可以很快地得到查询结果，不过在写方面，就慢得多
* LDAP提供了静态数据的快速查询方式
* Client/server模型，Server 用于存储数据，Client提供操作目录信息树的工具
* 这些工具可以将数据库的内容以文本格式（LDAP 数据交换格式，LDIF）呈现在您的面前
* LDAP是一种开放Internet标准，LDAP协议是跨平台的Interent协议

***

**特性：**

* **Entry**: 条目，记录项，LDAP的基本颗粒，LDAP添加、删除、更改、检索的基础；
* **Attribute**: 属性，每个条目都包含很多属性，用来记录其各种基本信息；
* **ObjectClass**: 对象类，属性的合集，可以被条目继承，每个条目可以继承多个对象类，重复属性只会保留一个；

***

* 分布式LDAP是以明文的格式通过网络发送信息的，所以通过使用SSL/TLS的加密协议来保证数据传送的保密性和完整性

***

* PyBossa使用LDAP进行用户验证：[PyBossa Tutorial](https://docs.pybossa.com/installation/configuration/#ldap_user_filter_field)，[LDAP身份验证通用步骤](https://blog.csdn.net/u014307117/article/details/65635073)

