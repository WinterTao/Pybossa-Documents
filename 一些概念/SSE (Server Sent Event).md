# SSE (Server Sent Event)

**参考**：[SSE教程](http://www.ruanyifeng.com/blog/2017/05/server-sent_events.html)

***

* 服务器通过发送流信息的方式向浏览器推送信息

***

**Python中的实现流程：**

* 需要配合Redis实现
* 调用Redis中的pubsub方法，返回一个PubSub对象
* PubSub对象有publish和subscribe方法
* publish会将消息发布在指定的channel中
* subscribe方法对指定channel进行订阅，当有消息发布在这个channel中时，从这个channel读取发布的消息
* 将收到的消息处理成一个生成器，通过flask中的Response类将其发布到stream中
* 前端通过读取stream的内容获得SSE的推送内容



