# Mac下部署Pybossa

## 安装Homebrew
Homebrew是macOS中很常用的包管理工具，为了方便之后各种组件的安装，建议先阿女Homebrew

```
ruby -e "$(curl -fsSL https://raw.github.com/Homebrew/homebrew/go/install)"
```

*如果安装后发现Homebrew在使用中下载速度比较慢，可以尝试换源，具体参考：[Homebrew换源](https://blog.csdn.net/jeikerxiao/article/details/72705629)

***

## 安装Git

```
brew install git
```

***

## 安装PostgreSQL

```
brew install postgresql
```

***

## 安装virtualenv (可选)

virtualenv主要用于创建一个独立的python虚拟环境，以防止系统的python环境被污染。虽然是非必须，不过还是建议对不同需求的python项目创建和使用单独的虚拟环境

安装：

```
sudo pip install virtualenv
```

***

## 安装Pybossa需要的Library

从git上下载pybossa项目源码并进入项目文件夹：

```
git clone --recursive https://github.com/Scifabric/pybossa
cd pybossa
```

开启虚拟环境(可选，建议开启):

```
virtualenv env
source env/bin/activate
```

通过requirment.txt下载并安装必要的Library

```
pip install -U pip
pip install -r requirements.txt
```

***

## 创建Pybossa项目相关配置文件

```
cp settings_local.py.tmpl settings_local.py
cp alembic.ini.template alembic.ini
```
*`settings_local.py`和`alembic.ini`中的内容可以按照需求修改,特别是`alembic.ini`中注意指明自己项目的数据库地址参数`sqlalchemy.url`

***

## 安装和启动Redis

### 安装:

```
brew install redis
```

### 启动
后续运行pybossa的时候是需要先启动redis-server和redis-sentinel

**启动redis-server:**

```
redis-server
```

**启动sentinel:**
sentinel需要配置文件才能启动，如果自行创建后缀为.conf的配置文件，可参考或者直接在文件中写入以下内容：

```
daemonize yes
sentinel monitor mymaster 127.0.0.1 6379 2
sentinel down-after-milliseconds mymaster 60000
sentinel failover-timeout mymaster 180000
sentinel parallel-syncs mymaster 1
```

或者直接使用pybossa源码中contrib文件夹下的配置文件sentinel.conf。

启动sentinel的命令如下（在pybossa项目文件夹根目录中）：

```
redis-server contrib/sentinel.conf --sentinel
```

***

## 设置Database
### 启动postgreSQL服务器

```
pg_ctl -D /usr/local/var/postgres/data -l /usr/local/var/postgres/log/log.log start
```

*如果没将数据库设为数据库服务设置为开机启动，在设备重启后运行项目之前记得先用这条命令开启数据库服务

### 添加用户

```
createuser -d -P [用户名]
```

这条命令之后会让你设置密码。设置的用户名记得要与alembic.ini中`sqlalchemy.url`参数后面的用户名和密码一致，文件中默认是`pybossa:tester`，其中pybossa是用户名，tester是密码

### 创建数据库

```
createdb [数据库名] -O [用户名]
```

创建后如果数据库名不是设置成alembic.ini中的数据库名`pybossa`，记得更改alembic.ini的`sqlalchemy.url`参数

```
sqlalchemy.url = postgresql://[用户名]:[密码]@localhost/[数据库名]
```

***

## 填充数据库

```
python cli.py db_create
```

***

## 运行

```
python run.py
```

打卡 http://0.0.0.0:5000 ，如果出现以下页面说明运行成功：
![Screen Shot 2018-05-10 at 2.45.38 P](media/15259136749247/Screen%20Shot%202018-05-10%20at%202.45.38%20PM.png)

***

## *完整运行步骤

**step1.** 开启redis-server

```
redis-server
```

**step2.** 开启sentinel

```
redis-server contrib/sentinel.conf --sentinel 
```

**step3.** 开启数据库

```
pg_ctl -D /usr/local/var/postgres/data -l /usr/local/var/postgres/log/log.log start
```

**step4.** 运行

```
python run.py
```





