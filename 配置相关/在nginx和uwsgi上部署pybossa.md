# 在nginx和uwsgi上部署pybossa

## 安装nginx

```
brew install nginx
```

***

## 安装uwsgi

建议通过虚拟python环境安装

```
pip install -U uwsgi
```

***

## 修改nginx配置文件

nginx配置文件为contrib/nginx/的`pybossa`文件：

* 将其中的`root`参数改为`[pybossa根目录]`
* 将第一个`if`后面条件中的路径改为`[pybossa根目录]/503.html`
* 将`location /static`中的`alias`后面的路径参数改为`[pybossa根目录]/pybossa/themes/default/static`

修改完成后再pybossa根目录下执行命令：

* 如果/usr/local/etc/nginx/中已经存在sites-enabled文件夹且里面有default配置文件，则先删除default文件：

```
sudo rm /usr/local/etc/nginx/sites-enabled/defualt
```

* 在/usr/local/etc/nginx/中创建sites-available文件夹和sites-enabled文件夹：

```
sudo mkdir -p /usr/local/etc/nginx/sites-available
sudo mkdir -p /usr/local/etc/nginx/sites-enabled
```

* 复制nginx配置文件到sites-available文件夹并连接到sites-enabled文件夹

```
sudo cp contrib/nginx/pybossa /usr/local/etc/nginx/sites-available/
sudo ln -s /usr/local/etc/nginx/sites-available/pybossa /usr/local/etc/bginx/sites-enabled/pybossa
```

* 修改nginx.conf配置文件

```
vi /usr/local/etc/nginx/nginx.conf
```

在`http{}`中添加语句

```
include /usr/local/etc/nginx/sites-enabled/*;
```

* 启动nginx服务

```
sudo nginx
```

如果想要停止nginx服务，可以使用命令：

```
sudo nginx -s stop
```

如果想重启nginx服务， 可以使用命令:

```
sudo nginx -s reload
```

***

## 修改uwsgi配置文件

先将contrib文件夹下的pybossa.ini.template模板复制到pybossa项目根目录下（命令在pybossa根目录下执行）：

```
cp contrib/pybossa.ini.template pybossa.ini
```

修改内容为:

```
[uwsgi]
socket = /tmp/pybossa.sock
chmod-socket = 666
chdir = [pybossa项目绝对路径]
pythonpath = ..
virtualenv = [python虚拟环境绝对路径]
module = run:app
cpu-affinity = 1
processes = 2
threads = 2
stats = /tmp/pybossa-stats.sock
buffer-size = 65535
```

***

## 安装supervisor

```
brew install supervisor
```

***supervisor默认为开机启动，若想取消开机启动，运行以下命令：**

```
launchctl unload -w ~/Library/LaunchAgent/homebrew.mxcl.redis.plist
```

### 配置supervisor

* 创建service配置文件存放路径

```
mkdir -p /usr/local/etc/supervisor.d
```

* 修改配置文件supervisord.ini

```
vi /usr/local/etc/supervisord.ini
```

* 将以下两行前面的`;`注释删掉

```
;[inter_http_server]
;port=127.0.0.1:9001
```

* 将最后一行的file参数设置为之前创建的service配置文件路径

```
[include]
files = /usr/local/etc/supervisor.d/*.ini
```

***

## 将Redis和sentinel配置为supervisor的services
### 停止redis相关进程
* 用ps命令查看进程id:

```
ps -ef | grep redis
```

* 出现的各条目第二项为进程id，结束掉除grep redis外所有相关进程

```
kill -9 [进程id]
```

* 关闭redis服务的开机启动（如果有）：

```
launchctl unload -w ~/Library/LaunchAgent/homebrew.mxcl.redis.plist
```

### 配置redis-server和sentinel关于supervisor的配置文件

* 将模板文件复制为.ini结尾的service配置文件

```
cp contrib/supervisor/redis-server.conf contrib/supervisor/redis-server.ini
cp contrib/supervisor/redis-sentinel.conf contrib/supervisor/redis-sentinel.ini
```

* 将redis-server.ini文件修改为:

```
[program:redis-server]
command=/usr/local/bin/redis-server /usr/local/etc/redis/redis.conf
autostart=true
autorestart=true
user=[你的用户名]
```

**\**记得修改[你的用户名]为你当前登录的用户名，后面的配置文件同理**

* 将redis-sentinel.ini文件修改为:

```
[program:redis-sentinel]
command=/usr/local/bin/redis-sentinel /usr/local/etc/redis/sentinel.conf
autostart=true
autorestart=true
user=[你的用户名]
```

* 修改contrib/redis-supervisor下redis.conf配置文件:
    
    * 将logfile后面的路径参数修改为`/usr/local/etc/redis/log/redis-server.log`
    * 将dir后面的路径参数修改为`/usr/local/var/db/redis/`
    * 创建log文件目录：
    
    ```
    mkdir -p /usr/local/etc/redis/log
    ```

* 复制redis-server.ini、redis-sentinel.ini、redis.conf和sentinel.conf到指定位置

```
sudo cp contrib/supervisor/redis-server.ini /usr/local/etc/supervisor.d/
sudo cp contrib/supervisor/redis-sentinel.ini /usr/local/etc/supervisor.d/
sudo cp contrib/redis-supervisor/redis.conf /usr/local/etc/redis/
sudo cp contrib/redis-supervisor/sentinel.conf /usr/local/etc/redis/
```

* 设置文件使用者

```
sudo chown [你的用户名] /usr/local/etc/redis/redis.conf
sudo chown [你的用户名] /usr/local/etc/redis/sentinel.conf
```

**\**其中[你的用户名]与前面的用户名保持一致**

* 重启supervisor

```
sudo brew services stop supervisor
sudo brew services start supervisor
```

或

```
sudo brew services restart supervisor
```

**可以用ps命令查看服务是否成功启动，如果成功启动，redis-server会显示占用6379端口，sentinel显示占用26379端口

```
ps -ef | grep redis
```

***

## 设置RQ-Scheduler和RQ-worker为supervisor的services

* 复制模板文件

```
cp contrib/supervisor/rq-scheduler.conf.template contrib/supervisor/rq-scheduler.ini
cp contrib/supervisor/rq-worker.conf.template contrib/supervisor/rq-worker.ini
```

* 修改rq-scheduler.ini为:

```
[program:rq-scheduler]
command=[pybossa根目录绝对路径]/env/bin/rqscheduler --host 127.0.0.1 --port 6379 --interval 60 --db 0
directory=[pybossa根目录绝对路径]
autostart=true
autorestart=true
priority=998
user=[你的用户名]
log_stdout=true
log_stderr=true
logfile=/var/log/rq-scheduler.log
logfile_maxbytes=10MB
logfile_backups=2
```

* 修改rq-worker.ini为:

```
[program:rq-worker]
command=[pybossa根目录绝对路径]/env/bin/python app_context_rqworker.py scheduled_jobs super high medium low email maintenance
directory=[pybossa根目录绝对路径]
autostart=true
autorestart=true
priority=997
user=[你的用户名]
log_stdout=true
log_stderr=true
logfile=/var/log/rq-worker.log
logfile_maxbytes=10MB
logfile_backups=2
```

* 重启supervisor

```
sudo brew services restart supervisor
```

**可用`ps -ef | grep rq`查看进程是否正常开启

***

## 设置Pybossa

因为我们准备接下来在nginx下运行Pybossa，所以先在setting_local.py文件中修改相应设置

* 注释掉HOST和PORT设置

```
# HOST = '0.0.0.0'
# PORT = 12000
```

* 设置server_name和端口

```
SERVER_NAME = '[你的server_name, 如：mypybossa.com]'
PORT = 80
```

* 如果你是在本地配置，需要将域名解析到本地ip地址，可以通过编辑hosts文件来实现

```
vi /etc/hosts
```

在里面插入一行

```
127.0.0.1   [你前面设置的server_name]
```

## 配置Pybossa为supervisor的一个service

* 复制模板

```
cp contrib/supervisor/pybossa.conf.template contrib/supervisor/pybossa.ini
```

* 修改contrib/supervisor文件夹中配置文件pybossa.ini为：

```
[program:pybossa]
command=[pybossa根目录绝对路径]/env/bin/uwsgi [pybossa根目录绝对路径]/pybossa.ini
directory=[pybossa根目录绝对路径]
autostart=true
autorestart=true
user=[你的用户名]
log_stdout=true
log_stderr=true
logfile=/var/log/pybossa.log
logfile_maxbytes=10MB
logfile_backups=2
```

* 复制配置文件到supervisor.d

```
sudo cp contrib/supervisor/pybossa.ini /usr/local/etc/supervisor.d/
```

* 重启supervisor

```
sudo brew services restart supervisor
```

之后通过设置的server_name（如：mypybossa.com）便可以直接访问项目主页

***

## *通过supervisorctl管理supervisor服务

在终端输入命令 `supervisorctl` 就可以进入supervisor的控制台

* 通过`start`、`stop`和`restart`加服务名可以开启、停止和重启服务
* `tail`加服务名可以查看服务日志
* `start all`、`stop all`和`restart all`可以开启、停止和重启全部服务

**也可以通过访问 http://0.0.0.0:9001 访问supervisor的图形管理界面**

***

## *运行

**step1: 开启数据库服务**

```
pg_ctl -D /usr/local/var/postgres/data -l /usr/local/var/postgres/log/log.log start
```

如果需要关闭

```
pg_ctl -D /usr/local/var/postgres/data stop -s -m fast
``` 

**step2: 开启supervisor中的服务**

```
sudo brew services start supervisor
```

**step3: 开启nginx服务**

```
sudo nginx
```


